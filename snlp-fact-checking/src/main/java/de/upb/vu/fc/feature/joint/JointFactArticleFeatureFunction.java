package de.upb.vu.fc.feature.joint;


import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.math.linearalgebra.Vector;
import de.upb.vu.fc.wikipedia.RawWikipediaArticle;


public interface JointFactArticleFeatureFunction {

   public Vector generateFeatureVector(Fact fact, RawWikipediaArticle article);


   public int getFeatureVectorLength();

}
