package de.upb.vu.fc.confidencescore.computer.ml;


import java.util.ArrayList;
import java.util.List;


public class TrainingDataset {

   private List<TrainingInstance> trainingInstances;


   public TrainingDataset(int size) {
      super();
      this.trainingInstances = new ArrayList<>(size);
   }


   public void addTrainingInstance(TrainingInstance fact) {
      trainingInstances.add(fact);
   }


   public List<TrainingInstance> getTrainingInstances() {
      return trainingInstances;
   }


   public int size() {
      return trainingInstances.size();
   }

}
