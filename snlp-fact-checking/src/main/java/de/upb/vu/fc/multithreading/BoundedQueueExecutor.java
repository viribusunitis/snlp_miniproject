package de.upb.vu.fc.multithreading;


import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.Semaphore;


public class BoundedQueueExecutor {
   private final ExceptionPrintingThreadPoolExecutor executor;
   private final Semaphore semaphore;


   public BoundedQueueExecutor(int numberOfThreads, int queueSize) {
      this.executor = new ExceptionPrintingThreadPoolExecutor(numberOfThreads);
      this.semaphore = new Semaphore(queueSize);
   }


   public void submitTask(final Runnable command) throws InterruptedException {
      semaphore.acquire();
      try {
         executor.execute(new Runnable() {
            @Override
            public void run() {
               try {
                  command.run();
               } finally {
                  semaphore.release();
               }
            }
         });
      } catch (RejectedExecutionException e) {
         semaphore.release();
      }
   }


   public void shutdown() {
      executor.shutdown();
   }
}
