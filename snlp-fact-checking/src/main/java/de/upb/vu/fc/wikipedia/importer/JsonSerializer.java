package de.upb.vu.fc.wikipedia.importer;


import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.upb.vu.fc.wikipedia.RawWikipediaArticle;


public class JsonSerializer {

   private ObjectMapper mapper;


   public JsonSerializer() {
      mapper = new ObjectMapper();
   }


   public RawWikipediaArticle deserializeJsonToArticle(String article) throws JsonParseException, JsonMappingException, IOException {
      return mapper.readValue(article, RawWikipediaArticle.class);
   }


   public String serializeArticleToJson(RawWikipediaArticle article) throws JsonProcessingException {
      return mapper.writeValueAsString(article);
   }
}
