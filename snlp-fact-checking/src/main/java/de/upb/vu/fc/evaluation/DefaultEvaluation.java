package de.upb.vu.fc.evaluation;


import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.upb.vu.fc.fact.FactChecker;
import de.upb.vu.fc.output.OutputGenerator;
import de.upb.vu.fc.wikipedia.importer.elastic.ElasticSearchException;


public class DefaultEvaluation implements IEvaluation {
   private Logger LOGGER = LoggerFactory.getLogger(DefaultEvaluation.class);


   @Override
   public double evaluate(EvaluationSetting setting) {
      FactChecker factChecker = new FactChecker(setting.getQueryFactory(), setting.getConfidenceScoreComputer(),
            setting.getConfidenceScoreAggregator());
      factChecker.train(setting.getTrainingDataset());

      try {
         List<Double> computedScores = factChecker.checkFacts(setting.getTestDataset());
         OutputGenerator.print(setting.getTestDataset(), computedScores, setting.getPathForOutputFile());
         return setting.getMetric().getLoss(setting.getTestDataset(), computedScores);
      } catch (ElasticSearchException e) {
         LOGGER.error("Could not check facts due to an elastic search problem.", e);
      } catch (IOException e) {
         LOGGER.error("Could not write fact results due to an IO problem.", e);
      }
      return -Double.MAX_VALUE;
   }

}
