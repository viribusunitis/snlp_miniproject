package de.upb.vu.fc.feature.article;

import de.upb.vu.fc.math.linearalgebra.Vector;
import de.upb.vu.fc.wikipedia.RawWikipediaArticle;

public interface ArticleFeatureFunction {
	
	public Vector generateFeatureVector(RawWikipediaArticle article);

}
