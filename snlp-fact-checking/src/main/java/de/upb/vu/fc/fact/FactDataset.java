package de.upb.vu.fc.fact;


import java.util.ArrayList;
import java.util.List;


public class FactDataset {

   private String name;

   private List<Fact> facts;


   public FactDataset(String name) {
      this(name, 10);
   }


   public FactDataset(String name, int size) {
      super();
      this.name = name;
      this.facts = new ArrayList<>(size);
   }


   public void addFact(Fact fact) {
      facts.add(fact);
   }


   public List<Fact> getFacts() {
      return facts;
   }


   public int getSize() {
      return facts.size();
   }


   public String getName() {
      return name;
   }


}
