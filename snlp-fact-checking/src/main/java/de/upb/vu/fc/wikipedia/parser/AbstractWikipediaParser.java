package de.upb.vu.fc.wikipedia.parser;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import org.apache.tools.bzip2.CBZip2InputStream;
import org.xml.sax.InputSource;

import de.upb.vu.fc.wikipedia.WikipediaArticleHandler;


public abstract class AbstractWikipediaParser {

   private URL wikipediaXMLDumpFileUrl = null;


   public AbstractWikipediaParser(File xmlDumpFile) throws MalformedURLException {
      wikipediaXMLDumpFileUrl = xmlDumpFile.toURI().toURL();
   }


   public abstract void addArticleHandler(WikipediaArticleHandler handler);


   public abstract void parse();


   public abstract void teardown();


   protected InputSource getInputSource() throws UnsupportedEncodingException, IOException {
      final BufferedReader br;
      if (wikipediaXMLDumpFileUrl.toExternalForm().endsWith(".gz")) {
         br = new BufferedReader(new InputStreamReader(new GZIPInputStream(wikipediaXMLDumpFileUrl.openStream()), "UTF-8"));
      } else if (wikipediaXMLDumpFileUrl.toExternalForm().endsWith(".bz2")) {
         InputStream fis = wikipediaXMLDumpFileUrl.openStream();
         byte[] ignoreBytes = new byte[2]; // "B", "Z" bytes from command line tools
         fis.read(ignoreBytes);
         br = new BufferedReader(new InputStreamReader(new CBZip2InputStream(fis), "UTF-8"));
      } else {
         br = new BufferedReader(new InputStreamReader(wikipediaXMLDumpFileUrl.openStream(), "UTF-8"));
      }

      return new InputSource(br);
   }
}
