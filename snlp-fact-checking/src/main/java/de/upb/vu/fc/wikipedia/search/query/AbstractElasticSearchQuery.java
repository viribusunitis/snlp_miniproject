package de.upb.vu.fc.wikipedia.search.query;


public abstract class AbstractElasticSearchQuery implements ElasticSearchQuery {

   private int offset;

   private int size;


   public AbstractElasticSearchQuery() {
      offset = 0;
      size = 10;
   }


   public AbstractElasticSearchQuery(int offset, int size) {
      this.offset = offset;
      this.size = size;
   }


   @Override
   public int getOffset() {
      return offset;
   }


   @Override
   public int getSize() {
      return size;
   }


}
