package de.upb.vu.fc.wikipedia.importer.elastic;


import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.upb.vu.fc.util.IOUtils;
import de.upb.vu.fc.wikipedia.WikipediaArticleHandler;
import de.upb.vu.fc.wikipedia.importer.MultiThreadingWikipediaArticleHandler;
import de.upb.vu.fc.wikipedia.importer.StatisticsCountingArticleHandler;
import de.upb.vu.fc.wikipedia.parser.WikipediaParser;
import de.upb.vu.fc.wikipedia.preprocessing.ArticleTextCleaningPreprocessor;
import de.upb.vu.fc.wikipedia.preprocessing.WikipediaArticlePreprocessor;


public class ElasticImportRunner {

   private static final Logger LOGGER = LoggerFactory.getLogger(ElasticImportRunner.class);

   private static final String XML_DUMP_FILE_PATH_ALEX = "/Users/alexanderhetzer/Downloads/enwiki-20171120-pages-articles.xml.bz2";
   private static final String XML_DUMP_FILE_PATH_TANJA = "/Users/Tanja/Development/Uni/enwiki-20171120-pages-articles.xml.bz2";
   private static final String XML_DUMP_FILE_PATH_SERVER = "/home/viribusunitis/data/enwiki-20171120-pages-articles.xml.bz2";

   private static int threads = 2;
   private static int documentsPerTask = 1_000;
   private static int queueSize = threads * 3;


   public static void main(String[] args) {
      if (args.length != 4) {
         LOGGER.info("Requiring 4 arguments: <s|h> <numThreads> <docsPerTask> <queueSize>");
         return;
      }
      String pathToXmlDump = args[0].equals("s") ? XML_DUMP_FILE_PATH_SERVER
            : (args[0].equals("h") ? XML_DUMP_FILE_PATH_ALEX : XML_DUMP_FILE_PATH_TANJA);

      threads = Integer.parseInt(args[1]);
      documentsPerTask = Integer.parseInt(args[2]);
      queueSize = Integer.parseInt(args[3]);

      WikipediaParser parser = null;
      String mappingCreateCommand;
      try {
         ElasticSearchIndexManager indexManager = new ElasticSearchIndexManager();
         indexManager.deleteIndexIfExists(ElasticSearchSettings.INDEX_NAME);
         mappingCreateCommand = IOUtils.readStringFromInputStream(
               ElasticImportRunner.class.getClassLoader().getResourceAsStream("elasticsearch/create_index_command.json"));
         LOGGER.info("Creating index with mapping: \n {}", mappingCreateCommand);
         indexManager.createIndex(ElasticSearchSettings.INDEX_NAME, ElasticSearchSettings.TYPE_NAME, mappingCreateCommand);
         indexManager.teardown();

         parser = new WikipediaParser(new File(pathToXmlDump));
         WikipediaArticlePreprocessor cleaningPreprocessor = new ArticleTextCleaningPreprocessor();
         WikipediaArticleHandler multiThreadingArticleHandler = new MultiThreadingWikipediaArticleHandler(threads, documentsPerTask,
               queueSize, Arrays.asList(cleaningPreprocessor));
         parser.addArticleHandler(multiThreadingArticleHandler);
         WikipediaArticleHandler statisticsCounter = new StatisticsCountingArticleHandler();
         parser.addArticleHandler(statisticsCounter);

         parser.parse();
      } catch (IOException e) {
         LOGGER.error("Encountered IOException: ", e);
      } catch (ElasticSearchException e) {
         LOGGER.error("Encountered Elasticsearch exception: ", e);
      } finally {
         if (parser != null) {
            parser.teardown();
         }
      }
   }
}
