package de.upb.vu.fc.wikipedia;


import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;


/***
 * FROM wikixmlj
 * 
 *
 */
public class SaxXmlHandler extends DefaultHandler {

   private static final String TEXT_TAG_QUALIFIED_NAME = "text";
   private static final String ID_TAG_QUALIFIED_NAME = "id";
   private static final String TITLE_TAG_QUALIFIED_NAME = "title";
   private static final String MEDIAWIKI_TAG_QUALIFIED_NAME = "mediawiki";
   private static final String REVISION_TAG_QUALIFIED_NAME = "revision";
   private static final String PAGE_TAG_QUALIFIED_NAME = "page";

   private List<WikipediaArticleHandler> articleHandlers;

   private boolean insideRevisionTag = false;
   private String currentTag;
   private StringBuilder currentArticleTextStringBuilder;
   private StringBuilder currentTitleStringBuilder;
   private StringBuilder currentIdStringBuilder;


   public SaxXmlHandler(List<WikipediaArticleHandler> articleHandlers) {
      this.articleHandlers = articleHandlers;
      currentArticleTextStringBuilder = new StringBuilder();
      currentTitleStringBuilder = new StringBuilder();
      currentIdStringBuilder = new StringBuilder();
   }


   @Override
   public void startElement(String uri, String name, String qName, Attributes attr) {
      currentTag = qName;
      if (qName.equals(PAGE_TAG_QUALIFIED_NAME)) {
         resetForNewArticle();
      } else if (qName.equals(REVISION_TAG_QUALIFIED_NAME)) {
         insideRevisionTag = true;
      }
   }


   private void resetForNewArticle() {
      currentArticleTextStringBuilder.setLength(0);
      currentTitleStringBuilder.setLength(0);
      currentIdStringBuilder.setLength(0);
   }


   @Override
   public void endElement(String uri, String name, String qName) {
      if (qName.equals(REVISION_TAG_QUALIFIED_NAME)) {
         insideRevisionTag = false;
      } else if (qName.equals(PAGE_TAG_QUALIFIED_NAME)) {
         RawWikipediaArticle article = createArticleFromCurrentData();
         articleHandlers.forEach(handler -> handler.handleArticle(article));
      } else if (qName.equals(MEDIAWIKI_TAG_QUALIFIED_NAME)) {
         // TODO do we need to handle this?
      }
   }


   private RawWikipediaArticle createArticleFromCurrentData() {
      String articleTile = currentTitleStringBuilder.toString();
      String articleId = currentIdStringBuilder.toString();
      String articleText = getCleanedTextOfCurrentArticle();
      RawWikipediaArticle article = new RawWikipediaArticle(articleTile, articleText, articleId);
      return article;
   }


   private String getCleanedTextOfCurrentArticle() {
      String rawArticleText = currentArticleTextStringBuilder.toString();
      return rawArticleText;
   }


   @Override
   public void characters(char ch[], int start, int length) {
      if (currentTag.equals(TITLE_TAG_QUALIFIED_NAME)) {
         currentTitleStringBuilder = currentTitleStringBuilder.append(ch, start, length);
      } else if ((currentTag.equals(ID_TAG_QUALIFIED_NAME)) && !insideRevisionTag) {
         // We only want the article and not the revision id
         currentIdStringBuilder.append(ch, start, length);
      } else if (currentTag.equals(TEXT_TAG_QUALIFIED_NAME)) {
         currentArticleTextStringBuilder = currentArticleTextStringBuilder.append(ch, start, length);
      }
   }
}
