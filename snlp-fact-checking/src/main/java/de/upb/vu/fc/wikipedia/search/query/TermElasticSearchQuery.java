package de.upb.vu.fc.wikipedia.search.query;


import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;


public class TermElasticSearchQuery extends AbstractElasticSearchQuery {

   private String fieldName;
   private String content;


   public TermElasticSearchQuery(String fieldName, String content) {
      this.fieldName = fieldName;
      this.content = content;
   }


   @Override
   public QueryBuilder getQueryBuilder() {
      return QueryBuilders.termQuery(fieldName, content);
   }


   @Override
   public QueryBuilder getPostFilter() {
      return null;
   }

}
