package de.upb.vu.fc.evaluation.metric;


import java.util.List;

import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.fact.FactDataset;


public class RootMeanSquareErrorMetric extends AbstractMetric {

   @Override
   public double getLoss(Fact fact, double computedScore) {
      return Math.sqrt(Math.pow(fact.getConfidenceScore() - computedScore, 2));
   }


   @Override
   public double getLoss(FactDataset factDataset, List<Double> computedScores) {
      if (factDataset.getSize() != computedScores.size()) {
         throw new IllegalArgumentException("Dataset and computed scores have a different size!");
      }

      double totalSquaredDifference = 0;
      for (int i = 0; i < factDataset.getSize(); i++) {
         double actualScore = factDataset.getFacts().get(i).getConfidenceScore();
         double computedScore = computedScores.get(i);
         totalSquaredDifference += Math.pow(actualScore - computedScore, 2);
      }
      totalSquaredDifference = totalSquaredDifference / factDataset.getSize();
      return Math.sqrt(totalSquaredDifference);
   }


}
