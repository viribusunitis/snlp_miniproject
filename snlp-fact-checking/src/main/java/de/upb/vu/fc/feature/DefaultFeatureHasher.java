package de.upb.vu.fc.feature;


import de.upb.vu.fc.math.linearalgebra.DenseDoubleVector;
import de.upb.vu.fc.math.linearalgebra.Vector;


public class DefaultFeatureHasher extends AbstractFeatureHasher {

   public DefaultFeatureHasher(int seed, int lengthOfResultingVector) {
      super(seed, lengthOfResultingVector);
   }


   @Override
   public Vector hash(String[] input) {
      Vector vector = new DenseDoubleVector(lengthOfResultingVector);
      for (int i = 0; i < input.length; i++) {
         int hashValue = hash(input[i]);
         int index = hashValue % lengthOfResultingVector;
         vector.setValue(index, vector.getValue(index) + 1);
      }
      return vector;
   }

}
