package de.upb.vu.fc.wikipedia.search.query.match;


import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.nlp.NamedEntityExtractor;
import de.upb.vu.fc.wikipedia.search.query.ElasticSearchQuery;
import de.upb.vu.fc.wikipedia.search.query.ElasticSearchQueryFactory;


public class ComplicatedMatchElasticSearchQueryFactory implements ElasticSearchQueryFactory {

   private NamedEntityExtractor namedEntityExtractor;


   public ComplicatedMatchElasticSearchQueryFactory() {
      namedEntityExtractor = new NamedEntityExtractor();
   }


   @Override
   public ElasticSearchQuery generateQuery(Fact fact) {
      return new ComplicatedMatchElasticSearchQuery(fact, namedEntityExtractor);
   }

}
