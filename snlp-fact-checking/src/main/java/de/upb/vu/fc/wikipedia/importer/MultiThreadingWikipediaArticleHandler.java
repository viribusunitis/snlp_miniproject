package de.upb.vu.fc.wikipedia.importer;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.upb.vu.fc.multithreading.BoundedQueueExecutor;
import de.upb.vu.fc.wikipedia.RawWikipediaArticle;
import de.upb.vu.fc.wikipedia.WikipediaArticleHandler;
import de.upb.vu.fc.wikipedia.preprocessing.WikipediaArticlePreprocessor;


public class MultiThreadingWikipediaArticleHandler implements WikipediaArticleHandler {

   private static final Logger LOGGER = LoggerFactory.getLogger(MultiThreadingWikipediaArticleHandler.class);

   private int currentTaskId;

   private int numberOfThreadsOfExecutorService;
   private int numberOfDocumentsPerIndexingTask;
   private int executorQueueSize;
   private List<WikipediaArticlePreprocessor> articlePreprocessors;

   private BoundedQueueExecutor executorService;

   private List<RawWikipediaArticle> articlesForNextWorker;


   public MultiThreadingWikipediaArticleHandler(int numberOfThreadsForWorkers, int numberOfDocumentsPerWorker, int executorQueueSize,
         List<WikipediaArticlePreprocessor> articlePreprocessors) {
      super();
      this.numberOfThreadsOfExecutorService = numberOfThreadsForWorkers;
      this.numberOfDocumentsPerIndexingTask = numberOfDocumentsPerWorker;
      this.executorQueueSize = executorQueueSize;
      this.articlePreprocessors = articlePreprocessors;
      executorService = new BoundedQueueExecutor(this.numberOfThreadsOfExecutorService, this.executorQueueSize);
      this.articlesForNextWorker = new ArrayList<>(numberOfDocumentsPerIndexingTask);
   }


   @Override
   public void handleArticle(RawWikipediaArticle article) {
      if (shouldArticleBeProcessed(article)) {
         articlesForNextWorker.add(article);

         if (articlesForNextWorker.size() >= numberOfDocumentsPerIndexingTask) {
            try {
               submitNextArticleIndexingTask();
            } catch (InterruptedException e) {
               LOGGER.error("Could not submit task due to Interrupted exception.", e);
            }
         }
      }
   }


   private void submitNextArticleIndexingTask() throws InterruptedException {
      WikipediaArticleIndexingTask task = new WikipediaArticleIndexingTask(currentTaskId, articlePreprocessors, articlesForNextWorker);
      executorService.submitTask(task);
      LOGGER.info("Submitting indexing task {}", currentTaskId);
      currentTaskId++;
      articlesForNextWorker.clear();
   }


   private boolean shouldArticleBeProcessed(RawWikipediaArticle article) {
      if (article.getText().startsWith("#REDIRECT")) {
         return false;
      }
      return true;
   }


   @Override
   public void teardown() {
      executorService.shutdown();
   }

}
