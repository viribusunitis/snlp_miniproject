package de.upb.vu.fc.evaluation;


import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.upb.vu.fc.confidencescore.aggregator.ConfidenceScoreAggregator;
import de.upb.vu.fc.confidencescore.aggregator.FirstDocumentScoreAggregator;
import de.upb.vu.fc.confidencescore.computer.ConfidenceScoreComputer;
import de.upb.vu.fc.confidencescore.computer.ml.MachineLearningBasedConfidenceScoreComputer;
import de.upb.vu.fc.evaluation.metric.Metric;
import de.upb.vu.fc.evaluation.metric.RootMeanSquareErrorMetric;
import de.upb.vu.fc.fact.FactDataset;
import de.upb.vu.fc.fact.FactDatasetReader;
import de.upb.vu.fc.feature.joint.CustomJointFeatureFunction;
import de.upb.vu.fc.feature.joint.JointFactArticleFeatureFunction;
import de.upb.vu.fc.machinelearning.MachineLearner;
import de.upb.vu.fc.machinelearning.weka.LogisticRegression;
import de.upb.vu.fc.wikipedia.search.query.ElasticSearchQueryFactory;
import de.upb.vu.fc.wikipedia.search.query.match.ComplicatedMatchElasticSearchQueryFactory;


public class EvaluationRunner {

   private static final Logger LOGGER = LoggerFactory.getLogger(EvaluationRunner.class);

   private static final String PATH_TO_OUTPUT_FILE = "evaluation/result.ttl";
   private static final String PATH_TO_TRAINING_DATASET = "data/confidence_score_computer_training_data.tsv";
   private static final String DEFAULT_PATH_TO_TRAINING_FACT_DATASET = "data/train.tsv";
   private static final String DEFAULT_PATH_TO_TEST_FACT_DATASET = "data/test.tsv";

   private static FactDataset trainingDataset;
   private static FactDataset testDataset;


   public static void main(String[] args) throws IOException {
      String pathToTestDataset = DEFAULT_PATH_TO_TEST_FACT_DATASET;
      if (args.length == 1) {
         pathToTestDataset = args[0];
      } else {
         LOGGER.warn("Running with default parameters. Expecting test dataset at: " + DEFAULT_PATH_TO_TEST_FACT_DATASET);
      }

      ElasticSearchQueryFactory queryFactory = new ComplicatedMatchElasticSearchQueryFactory();

      JointFactArticleFeatureFunction jointFeatureFunction = new CustomJointFeatureFunction();

      int featureVectorLength = jointFeatureFunction.getFeatureVectorLength();
      MachineLearner machineLearner = new LogisticRegression(featureVectorLength);

      ConfidenceScoreComputer confidenceScoreComputer = new MachineLearningBasedConfidenceScoreComputer(machineLearner,
            jointFeatureFunction, PATH_TO_TRAINING_DATASET);
      ConfidenceScoreAggregator confidenceScoreAggregator = new FirstDocumentScoreAggregator();

      Metric metric = new RootMeanSquareErrorMetric();

      initializeDatasets(pathToTestDataset);

      EvaluationSetting evaluationSetting = new EvaluationSetting(queryFactory, confidenceScoreComputer, confidenceScoreAggregator, metric,
            trainingDataset, testDataset, PATH_TO_OUTPUT_FILE);

      IEvaluation evaluation = new DefaultEvaluation();
      double evaluationResult = evaluation.evaluate(evaluationSetting);
      LOGGER.info("Evaluaton result with metric {}: {}", metric.toString(), evaluationResult);
   }


   private static void initializeDatasets(String pathToTestDataset) throws IOException {
      trainingDataset = getTrainingDataset();
      testDataset = getTestDataset(pathToTestDataset);
   }


   private static FactDataset getTrainingDataset() throws IOException {
      FactDatasetReader datasetReader = new FactDatasetReader();
      InputStream factTrainingDatasetInputstream = EvaluationRunner.class.getClassLoader()
            .getResourceAsStream(DEFAULT_PATH_TO_TRAINING_FACT_DATASET);
      return datasetReader.readTrainingDataset(factTrainingDatasetInputstream);
   }


   private static FactDataset getTestDataset(String pathToTestDataset) throws IOException {
      FactDatasetReader datasetReader = new FactDatasetReader();
      return datasetReader.readTestDataset(pathToTestDataset);
   }
}
