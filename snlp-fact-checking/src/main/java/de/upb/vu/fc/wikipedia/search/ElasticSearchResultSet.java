package de.upb.vu.fc.wikipedia.search;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import de.upb.vu.fc.wikipedia.RawWikipediaArticle;
import de.upb.vu.fc.wikipedia.importer.JsonSerializer;


public class ElasticSearchResultSet {

   private JsonSerializer jsonSerializer;
   private SearchResponse searchResponse;
   private List<ElasticSearchResult> results;


   public ElasticSearchResultSet(SearchResponse searchResponse) throws JsonParseException, JsonMappingException, IOException {
      this.searchResponse = searchResponse;
      this.jsonSerializer = new JsonSerializer();
      initializeFromSearchResponse();
   }


   private void initializeFromSearchResponse() throws JsonParseException, JsonMappingException, IOException {
      SearchHits searchHits = searchResponse.getHits();
      results = new ArrayList<>((int) searchHits.getTotalHits());
      for (SearchHit searchHit : searchHits) {
         String documentAsJson = searchHit.getSourceAsString();
         RawWikipediaArticle rawWikipediaArticle = jsonSerializer.deserializeJsonToArticle(documentAsJson);
         results.add(new ElasticSearchResult(rawWikipediaArticle, searchHit.getScore()));
      }
   }


   public List<ElasticSearchResult> getResults() {
      return results;
   }


   public int getNumberOfResults() {
      return results.size();
   }
}
