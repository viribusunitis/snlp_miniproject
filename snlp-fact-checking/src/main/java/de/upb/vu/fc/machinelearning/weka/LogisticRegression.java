package de.upb.vu.fc.machinelearning.weka;


import weka.classifiers.functions.Logistic;


public class LogisticRegression extends AbstractWekaMachineLearner {

   public LogisticRegression(int amountOfAttributes) {
      super(new Logistic(), amountOfAttributes, 2);
   }

}
