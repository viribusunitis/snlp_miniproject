package de.upb.vu.fc.wikipedia.search;


import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import de.upb.vu.fc.wikipedia.importer.elastic.ElasticSearchException;
import de.upb.vu.fc.wikipedia.importer.elastic.ElasticSearchSettings;
import de.upb.vu.fc.wikipedia.search.query.ElasticSearchQuery;


public class DefaultElasticSearchQueryExecutor implements ElasticSearchQueryExecutor {

   private TransportClient elasticClient;


   public DefaultElasticSearchQueryExecutor() throws UnknownHostException {
      setupElasticClient();
   }


   @Override
   public ElasticSearchResultSet executeQuery(ElasticSearchQuery searchQuery) throws ElasticSearchException {
      SearchResponse searchResponse = elasticClient //
            .prepareSearch(ElasticSearchSettings.INDEX_NAME) //
            .setTypes(ElasticSearchSettings.TYPE_NAME) //
            .setSearchType(SearchType.DFS_QUERY_THEN_FETCH) //
            .setQuery(searchQuery.getQueryBuilder()) //
            .setPostFilter(searchQuery.getPostFilter()) //
            .setFrom(searchQuery.getOffset()).setSize(searchQuery.getSize()).setExplain(true).get();
      try {
         return new ElasticSearchResultSet(searchResponse);
      } catch (IOException e) {
         throw new ElasticSearchException(String.format("Could not read search response %s.", searchResponse.toString()), e);
      }
   }


   private void setupElasticClient() throws UnknownHostException {
      Settings settings = Settings.builder().put("cluster.name", ElasticSearchSettings.CLUSTER_NAME)
            .put("node.name", ElasticSearchSettings.NODE_NAME).build();
      elasticClient = new PreBuiltTransportClient(settings).addTransportAddress(
            new TransportAddress(InetAddress.getByName(ElasticSearchSettings.IP), ElasticSearchSettings.TRANSPORT_CLIENT_PORT));
   }


   @Override
   public void teardown() {
      teardownElasticClient();
   }


   private void teardownElasticClient() {
      elasticClient.close();
   }

}
