package de.upb.vu.fc.evaluation.metric;


import java.util.List;

import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.fact.FactDataset;


public interface Metric {


   public double getLoss(Fact fact, double computedScore);


   public double getLoss(FactDataset factDataset, List<Double> computedScores);


}
