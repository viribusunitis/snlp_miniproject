package de.upb.vu.fc.wikipedia;


public interface WikipediaArticleHandler {

   public void handleArticle(RawWikipediaArticle article);


   public void teardown();
}
