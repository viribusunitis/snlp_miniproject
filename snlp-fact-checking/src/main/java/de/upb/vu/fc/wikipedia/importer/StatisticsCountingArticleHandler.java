package de.upb.vu.fc.wikipedia.importer;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.upb.vu.fc.wikipedia.RawWikipediaArticle;
import de.upb.vu.fc.wikipedia.WikipediaArticleHandler;


public class StatisticsCountingArticleHandler implements WikipediaArticleHandler {

   private static final Logger LOGGER = LoggerFactory.getLogger(StatisticsCountingArticleHandler.class);

   private static final double SAMPLE_SIZE = 100_000;
   private long totalAmountOfProcessedDocuments = 0;
   private long totalAmountOfRealReadDocuments = 0;
   private long amountOfProcessedDocumentsSinceLastMeasure = 0;
   private long lastStartTime = 0;


   @Override
   public void handleArticle(RawWikipediaArticle article) {
      if (totalAmountOfProcessedDocuments % SAMPLE_SIZE == 0) {
         if (lastStartTime > 0) {
            long timeNeededSinceLastMeasure = System.currentTimeMillis() - lastStartTime;
            LOGGER.debug("Average amount of seconds per document: "
                  + (timeNeededSinceLastMeasure / SAMPLE_SIZE) / amountOfProcessedDocumentsSinceLastMeasure);
            LOGGER.debug("Average amount of docments per second: "
                  + amountOfProcessedDocumentsSinceLastMeasure / (timeNeededSinceLastMeasure / SAMPLE_SIZE));
         }
         lastStartTime = System.currentTimeMillis();
         amountOfProcessedDocumentsSinceLastMeasure = 0;
         LOGGER.info("Read: {}", totalAmountOfProcessedDocuments);
         LOGGER.info("Read real documents: {}", totalAmountOfRealReadDocuments);
      }
      if (shouldArticleBeCountedAsRealArticle(article)) {
         totalAmountOfRealReadDocuments++;
      }
      totalAmountOfProcessedDocuments++;
      amountOfProcessedDocumentsSinceLastMeasure++;
   }


   private boolean shouldArticleBeCountedAsRealArticle(RawWikipediaArticle article) {
      if (article.getText().startsWith("#REDIRECT")) {
         return false;
      }
      return true;
   }


   @Override
   public void teardown() {
      LOGGER.debug("Finished import! \nTotal read documets: {} \nReal read documents: {}", totalAmountOfProcessedDocuments,
            totalAmountOfRealReadDocuments);
   }

}
