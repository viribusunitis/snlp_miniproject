package de.upb.vu.fc.feature.fact;


import java.util.Collection;

import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.feature.DefaultFeatureHasher;
import de.upb.vu.fc.math.linearalgebra.Vector;
import de.upb.vu.fc.nlp.NGramCreator;


public class FactNGramHashedFeatureFunction implements FactFeatureFunction {

   private DefaultFeatureHasher featureHasher;
   private NGramCreator nGramCreator;


   public FactNGramHashedFeatureFunction(int seed, int lengthOfResultingVector, int minimumGramLength, int maximumGramLength) {
      this.featureHasher = new DefaultFeatureHasher(seed, lengthOfResultingVector);
      this.nGramCreator = new NGramCreator(minimumGramLength, maximumGramLength);
   }


   @Override
   public Vector generateFeatureVector(Fact fact) {
      Collection<String> extractedNGrams = nGramCreator.extractNGrams(fact.getSentence());
      String[] input = extractedNGrams.toArray(new String[extractedNGrams.size()]);
      return featureHasher.hash(input);
   }


}
