package de.upb.vu.fc.output;


public class ValueScaler {

   public static double scalueUnitInvervalValueToNegative1Positive1(double value) {
      return scale(value, 0, 1, -1, 1);
   }


   public static double scale(final double value, final double baseMin, final double baseMax, final double limitMin,
         final double limitMax) {
      return ((limitMax - limitMin) * (value - baseMin) / (baseMax - baseMin)) + limitMin;
   }
}
