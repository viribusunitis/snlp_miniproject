package de.upb.vu.fc.evaluation;


import de.upb.vu.fc.confidencescore.aggregator.ConfidenceScoreAggregator;
import de.upb.vu.fc.confidencescore.computer.ConfidenceScoreComputer;
import de.upb.vu.fc.evaluation.metric.Metric;
import de.upb.vu.fc.fact.FactDataset;
import de.upb.vu.fc.wikipedia.search.query.ElasticSearchQueryFactory;


public class EvaluationSetting {

   private ElasticSearchQueryFactory queryFactory;
   private ConfidenceScoreComputer confidenceScoreComputer;
   private ConfidenceScoreAggregator confidenceScoreAggregator;

   private Metric metric;

   private FactDataset trainingDataset;
   private FactDataset testDataset;

   private String pathForOutputFile;


   public EvaluationSetting(ElasticSearchQueryFactory queryFactory, ConfidenceScoreComputer confidenceScoreComputer,
         ConfidenceScoreAggregator confidenceScoreAggregator, Metric metric, FactDataset trainingDataset, FactDataset testDataset,
         String pathForOutputFile) {
      super();
      this.queryFactory = queryFactory;
      this.confidenceScoreComputer = confidenceScoreComputer;
      this.confidenceScoreAggregator = confidenceScoreAggregator;
      this.metric = metric;
      this.trainingDataset = trainingDataset;
      this.testDataset = testDataset;
      this.pathForOutputFile = pathForOutputFile;
   }


   public ConfidenceScoreComputer getConfidenceScoreComputer() {
      return confidenceScoreComputer;
   }


   public ConfidenceScoreAggregator getConfidenceScoreAggregator() {
      return confidenceScoreAggregator;
   }


   public Metric getMetric() {
      return metric;
   }


   public FactDataset getTrainingDataset() {
      return trainingDataset;
   }


   public FactDataset getTestDataset() {
      return testDataset;
   }


   public String getPathForOutputFile() {
      return pathForOutputFile;
   }


   public ElasticSearchQueryFactory getQueryFactory() {
      return queryFactory;
   }


}
