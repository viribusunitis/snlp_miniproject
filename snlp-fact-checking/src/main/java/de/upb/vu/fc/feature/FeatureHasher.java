package de.upb.vu.fc.feature;


import de.upb.vu.fc.math.linearalgebra.Vector;


public interface FeatureHasher {

   public Vector hash(String[] input);

}
