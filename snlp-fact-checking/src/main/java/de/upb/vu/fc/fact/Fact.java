package de.upb.vu.fc.fact;


public class Fact {

   private String id;
   private String sentence;

   private double confidenceScore;


   public Fact(String id, String sentence) {
      super();
      this.id = id;
      this.sentence = sentence;
   }


   public Fact(String id, String sentence, double confidenceScore) {
      super();
      this.id = id;
      this.sentence = sentence;
      this.confidenceScore = confidenceScore;
   }


   public String getId() {
      return id;
   }


   public String getSentence() {
      return sentence;
   }


   public double getConfidenceScore() {
      return confidenceScore;
   }


   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      long temp;
      temp = Double.doubleToLongBits(confidenceScore);
      result = prime * result + (int) (temp ^ (temp >>> 32));
      result = prime * result + ((id == null) ? 0 : id.hashCode());
      result = prime * result + ((sentence == null) ? 0 : sentence.hashCode());
      return result;
   }


   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      Fact other = (Fact) obj;
      if (Double.doubleToLongBits(confidenceScore) != Double.doubleToLongBits(other.confidenceScore)) {
         return false;
      }
      if (id == null) {
         if (other.id != null) {
            return false;
         }
      } else if (!id.equals(other.id)) {
         return false;
      }
      if (sentence == null) {
         if (other.sentence != null) {
            return false;
         }
      } else if (!sentence.equals(other.sentence)) {
         return false;
      }
      return true;
   }


   @Override
   public String toString() {
      return "Fact [id=" + id + ", sentence=" + sentence + ", confidenceScore=" + confidenceScore + "]";
   }

}
