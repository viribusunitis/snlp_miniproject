package de.upb.vu.fc.confidencescore.computer.ml;


import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.fact.FactDataset;
import de.upb.vu.fc.util.IOUtils;
import de.upb.vu.fc.util.StringUtil;
import de.upb.vu.fc.wikipedia.RawWikipediaArticle;
import de.upb.vu.fc.wikipedia.importer.elastic.ElasticSearchException;
import de.upb.vu.fc.wikipedia.importer.elastic.ElasticSearchSettings;
import de.upb.vu.fc.wikipedia.search.DefaultElasticSearchQueryExecutor;
import de.upb.vu.fc.wikipedia.search.ElasticSearchQueryExecutor;
import de.upb.vu.fc.wikipedia.search.ElasticSearchResultSet;
import de.upb.vu.fc.wikipedia.search.query.TermElasticSearchQuery;


public class TrainingDatasetReader {

   private static final Logger LOGGER = LoggerFactory.getLogger(TrainingDatasetReader.class);

   private FactDataset dataset;
   private ElasticSearchQueryExecutor executor;


   public TrainingDatasetReader(FactDataset dataset) {
      this.dataset = dataset;
      try {
         this.executor = new DefaultElasticSearchQueryExecutor();
      } catch (UnknownHostException e) {
         throw new RuntimeException("DefaultElasticSearchQueryExecutor could not be created.");
      }
   }


   public TrainingDataset read(String pathToTrainingDatasetFile) throws IOException {
      String[] fileContent = IOUtils.readStringFromInputStream(getClass().getClassLoader().getResourceAsStream(pathToTrainingDatasetFile))
            .split("\\n");

      TrainingDataset dataset = new TrainingDataset(fileContent.length);
      for (String line : fileContent) {
         if (!line.startsWith("#")) {
            String[] splittedLine = line.split(StringUtil.REGEX_WHITESPACE);
            if (splittedLine.length != 3) {
               throw new RuntimeException("Training dataset file" + pathToTrainingDatasetFile + "could not be read. (" + line + ")("
                     + Arrays.toString(splittedLine) + ")");
            }

            try {
               TrainingInstance instance = new TrainingInstance(getFact(splittedLine[0]), getArticle(splittedLine[1]),
                     Double.parseDouble(splittedLine[2]));
               dataset.addTrainingInstance(instance);
            } catch (ElasticSearchException e) {
               LOGGER.error("Was not able to read instance with factId={} and articleId={}", splittedLine[0], splittedLine[1], e);
               // do nothing.
            }
         }
      }
      return dataset;
   }


   private Fact getFact(String id) {
      Optional<Fact> fact = dataset.getFacts().stream().filter(f -> f.getId().equals(id)).findFirst();
      if (fact.isPresent()) {
         return fact.get();
      }
      throw new RuntimeException("Fact with id=" + id + " is unknown.");
   }


   private RawWikipediaArticle getArticle(String id) throws ElasticSearchException {
      ElasticSearchResultSet resultSet = executor
            .executeQuery(new TermElasticSearchQuery(ElasticSearchSettings.ID_FIELD_NAME, String.valueOf(id)));
      if (resultSet.getResults().isEmpty()) {
         throw new ElasticSearchException("The resultSet is empty.");
      }
      return resultSet.getResults().get(0).getArticle();
   }

}
