package de.upb.vu.fc.wikipedia.search.query.match;


import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.wikipedia.search.query.ElasticSearchQuery;
import de.upb.vu.fc.wikipedia.search.query.ElasticSearchQueryFactory;


public class SimpleMatchElasticSearchQueryFactory implements ElasticSearchQueryFactory {

   @Override
   public ElasticSearchQuery generateQuery(Fact fact) {
      return new SimpleMatchElasticSearchQuery(fact);
   }

}
