package de.upb.vu.fc.output;


import java.io.File;
import java.io.IOException;
import java.util.List;

import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.fact.FactDataset;
import de.upb.vu.fc.util.IOUtils;
import de.upb.vu.fc.util.StringUtil;


public class OutputGenerator {

   public static void print(FactDataset dataset, List<Double> confidenceScores, String pathToFile) throws IOException {
      StringBuilder stringBuilder = new StringBuilder();
      for (int i = 0; i < dataset.getSize(); i++) {
         stringBuilder.append(getFactUri(dataset.getFacts().get(i)));
         stringBuilder.append(StringUtil.TAB_SPACE);
         stringBuilder.append(getPropUri());
         stringBuilder.append(StringUtil.TAB_SPACE);
         stringBuilder.append(getTruthValue(confidenceScores.get(i)));
         stringBuilder.append(getTruthValueType());
         stringBuilder.append(StringUtil.DOT);
         stringBuilder.append(StringUtil.LINE_BREAK);
      }

      IOUtils.writeToFileCreatingMissingDirectories(new File(pathToFile), stringBuilder.toString());
   }


   private static String getFactUri(Fact fact) {
      return "<http://swc2017.aksw.org/task2/dataset/" + fact.getId() + ">";
   }


   private static String getPropUri() {
      return "<http://swc2017.aksw.org/hasTruthValue>";
   }


   private static String getTruthValue(double truthValue) {
      return "\"" + ValueScaler.scalueUnitInvervalValueToNegative1Positive1(truthValue) + "\"";
   }


   private static String getTruthValueType() {
      return "^^<http://www.w3.org/2001/XMLSchema#double>";
   }

}
