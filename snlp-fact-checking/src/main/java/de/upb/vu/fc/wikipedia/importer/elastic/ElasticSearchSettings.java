package de.upb.vu.fc.wikipedia.importer.elastic;


public class ElasticSearchSettings {
   public static final String IP = "localhost";
   public static final int TRANSPORT_CLIENT_PORT = 9300;

   public static final String CLUSTER_NAME = "viribus-unitis-fc-cluster";
   public static final String NODE_NAME = "vu-fc-node-1";

   public static final String INDEX_NAME = "wikipedia";
   public static final String TYPE_NAME = "article";

   public static final String ID_FIELD_NAME = "id";
   public static final String TITLE_FIELD_NAME = "title";
   public static final String TEXT_FIELD_NAME = "text";

}
