package de.upb.vu.fc.fact;


import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.upb.vu.fc.confidencescore.aggregator.ConfidenceScoreAggregator;
import de.upb.vu.fc.confidencescore.computer.ConfidenceScoreComputer;
import de.upb.vu.fc.util.datastructure.Pair;
import de.upb.vu.fc.wikipedia.importer.elastic.ElasticSearchException;
import de.upb.vu.fc.wikipedia.search.DefaultElasticSearchQueryExecutor;
import de.upb.vu.fc.wikipedia.search.ElasticSearchQueryExecutor;
import de.upb.vu.fc.wikipedia.search.ElasticSearchResult;
import de.upb.vu.fc.wikipedia.search.ElasticSearchResultSet;
import de.upb.vu.fc.wikipedia.search.query.ElasticSearchQuery;
import de.upb.vu.fc.wikipedia.search.query.ElasticSearchQueryFactory;


public class FactChecker {

   private Logger LOGGER = LoggerFactory.getLogger(FactChecker.class);

   private ElasticSearchQueryFactory queryFactory;
   private ElasticSearchQueryExecutor queryExecutor;

   private ConfidenceScoreComputer confidenceScoreComputer;
   private ConfidenceScoreAggregator confidenceScoreAggregator;


   public FactChecker(ElasticSearchQueryFactory queryFactory, ConfidenceScoreComputer confidenceScoreComputer,
         ConfidenceScoreAggregator confidenceScoreAggregator) {
      super();
      this.queryFactory = queryFactory;
      this.confidenceScoreComputer = confidenceScoreComputer;
      this.confidenceScoreAggregator = confidenceScoreAggregator;
      initialize();
   }


   private void initialize() {
      try {
         queryExecutor = new DefaultElasticSearchQueryExecutor();
      } catch (UnknownHostException e) {
         throw new RuntimeException(e);
      }
   }


   public void train(FactDataset trainingDataset) {
      LOGGER.debug("Starting training of fact checker.");
      confidenceScoreComputer.setup(trainingDataset);
      LOGGER.debug("Finished training of fact checker.");
   }


   public List<Double> checkFacts(FactDataset dataset) throws ElasticSearchException {
      LOGGER.debug("Starting fact checking.");
      List<Double> results = new ArrayList<>(dataset.getSize());
      for (Fact fact : dataset.getFacts()) {
         double computedScore = checkFact(fact);
         results.add(computedScore);
      }
      LOGGER.debug("Finished fact checking.");
      return results;
   }


   private double checkFact(Fact fact) throws ElasticSearchException {
      ElasticSearchQuery searchQuery = queryFactory.generateQuery(fact);
      ElasticSearchResultSet resultSet = queryExecutor.executeQuery(searchQuery);

      LOGGER.debug("Checking fact {}.", fact);
      List<Pair<ElasticSearchResult, Double>> resultAndScoreList = new ArrayList<>(resultSet.getNumberOfResults());
      for (ElasticSearchResult result : resultSet.getResults()) {
         LOGGER.trace("Processing article {}.", result.getArticle());
         double computedScore = confidenceScoreComputer.computeScore(fact, result);
         resultAndScoreList.add(Pair.of(result, computedScore));
      }

      return confidenceScoreAggregator.aggregateScores(fact, resultAndScoreList);
   }
}
