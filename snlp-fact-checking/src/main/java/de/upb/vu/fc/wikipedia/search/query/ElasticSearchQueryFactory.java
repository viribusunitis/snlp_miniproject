package de.upb.vu.fc.wikipedia.search.query;


import de.upb.vu.fc.fact.Fact;


public interface ElasticSearchQueryFactory {

   public ElasticSearchQuery generateQuery(Fact fact);
}
