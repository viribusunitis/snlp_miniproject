package de.upb.vu.fc.confidencescore.computer;


import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.fact.FactDataset;
import de.upb.vu.fc.wikipedia.search.ElasticSearchResult;


public interface ConfidenceScoreComputer {

   public void setup(FactDataset trainingDataset);


   public double computeScore(Fact fact, ElasticSearchResult result);

}
