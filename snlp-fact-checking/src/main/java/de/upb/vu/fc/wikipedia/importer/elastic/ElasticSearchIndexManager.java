package de.upb.vu.fc.wikipedia.importer.elastic;


import java.net.InetAddress;
import java.net.UnknownHostException;

import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ElasticSearchIndexManager {

   private static final Logger LOGGER = LoggerFactory.getLogger(ElasticSearchIndexManager.class);

   private TransportClient elasticClient;
   private IndicesAdminClient indexManipulationClient;


   public ElasticSearchIndexManager() throws UnknownHostException {
      setupElasticClient();
   }


   public void createIndex(String indexName, String typeName, String mappingCommand) throws ElasticSearchException {
      CreateIndexResponse indexCreationResponse = indexManipulationClient.prepareCreate(indexName)
            .addMapping(typeName, mappingCommand, XContentType.JSON).get();
      if (!indexCreationResponse.isAcknowledged()) {
         String exceptionMessage = String.format("Could not create index %s. Error: %s", indexName, indexCreationResponse.toString());
         throw new ElasticSearchException(exceptionMessage);
      }
   }


   public void deleteIndexIfExists(String indexName) throws ElasticSearchException {
      boolean indexExists = indexManipulationClient.prepareExists(indexName).get().isExists();
      if (indexExists) {
         LOGGER.debug("Deleting ES index {}.", indexName);
         DeleteIndexResponse deletionResponse = indexManipulationClient.prepareDelete(indexName).get();
         if (!deletionResponse.isAcknowledged()) {
            String exceptionMessage = String.format("Could not delete index %s. Error: %s", indexName, deletionResponse.toString());
            throw new ElasticSearchException(exceptionMessage);
         }
      }
   }


   private void setupElasticClient() throws UnknownHostException {
      Settings settings = Settings.builder().put("cluster.name", ElasticSearchSettings.CLUSTER_NAME)
            .put("node.name", ElasticSearchSettings.NODE_NAME).build();
      elasticClient = new PreBuiltTransportClient(settings).addTransportAddress(
            new TransportAddress(InetAddress.getByName(ElasticSearchSettings.IP), ElasticSearchSettings.TRANSPORT_CLIENT_PORT));
      indexManipulationClient = elasticClient.admin().indices();
   }


   public void teardown() {
      teardownElasticClient();
   }


   private void teardownElasticClient() {
      elasticClient.close();
   }
}
