package de.upb.vu.fc.evaluation;


public interface IEvaluation {

   public double evaluate(EvaluationSetting setting);
}
