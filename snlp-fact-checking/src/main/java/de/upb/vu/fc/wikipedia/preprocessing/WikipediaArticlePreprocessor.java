package de.upb.vu.fc.wikipedia.preprocessing;

import de.upb.vu.fc.wikipedia.RawWikipediaArticle;

public interface WikipediaArticlePreprocessor {

   public void process(RawWikipediaArticle article);


   public WikipediaArticlePreprocessor copy();

}
