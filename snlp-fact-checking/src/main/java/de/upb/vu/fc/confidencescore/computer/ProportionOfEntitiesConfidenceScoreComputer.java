package de.upb.vu.fc.confidencescore.computer;


import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.fact.FactDataset;
import de.upb.vu.fc.nlp.NamedEntityExtractor;
import de.upb.vu.fc.nlp.NamedEntityInfo;
import de.upb.vu.fc.util.StringUtil;
import de.upb.vu.fc.wikipedia.search.ElasticSearchResult;


public class ProportionOfEntitiesConfidenceScoreComputer implements ConfidenceScoreComputer {

   private NamedEntityExtractor namedEntityExtractor;

   private NamedEntityInfo factNamedEntityInfo;
   private String cleanedArticleText;


   public ProportionOfEntitiesConfidenceScoreComputer() {
      namedEntityExtractor = new NamedEntityExtractor();
   }


   @Override
   public void setup(FactDataset trainingDataset) {

   }


   @Override
   public double computeScore(Fact fact, ElasticSearchResult result) {
      factNamedEntityInfo = namedEntityExtractor.extractEntities(fact.getSentence());
      cleanedArticleText = result.getArticle().getText().replaceAll(StringUtil.REGEX_PUNCTUATION, StringUtil.EMPTY_STRING)
            .replaceAll("\n", StringUtil.SINGLE_WHITESPACE).toLowerCase();
      double percentageSum = getPercentageOfIncludedLocationsFeature() + getPercentageOfIncludedPersons()
            + getPercentageOfIncludedOrganizationsFeature();
      return percentageSum / 3;
   }


   private double getPercentageOfIncludedPersons() {
      if (factNamedEntityInfo.getPersons().size() == 0) {
         return 1;
      }
      int amountOfPersonsIncluded = 0;
      for (String person : factNamedEntityInfo.getPersons()) {
         if (cleanedArticleText.contains(person.toLowerCase())) {
            amountOfPersonsIncluded++;
         }
      }
      return amountOfPersonsIncluded / (double) factNamedEntityInfo.getPersons().size();
   }


   private double getPercentageOfIncludedLocationsFeature() {
      if (factNamedEntityInfo.getLocations().size() == 0) {
         return 1;
      }
      int amountOfLocationsIncluded = 0;
      for (String location : factNamedEntityInfo.getLocations()) {
         if (cleanedArticleText.contains(location.toLowerCase())) {
            amountOfLocationsIncluded++;
         }
      }
      return amountOfLocationsIncluded / (double) factNamedEntityInfo.getLocations().size();
   }


   private double getPercentageOfIncludedOrganizationsFeature() {
      if (factNamedEntityInfo.getOrganizations().size() == 0) {
         return 1;
      }
      int amountOfOrganizationsIncluded = 0;
      for (String organization : factNamedEntityInfo.getOrganizations()) {
         if (cleanedArticleText.contains(organization.toLowerCase())) {
            amountOfOrganizationsIncluded++;
         }
      }
      return amountOfOrganizationsIncluded / (double) factNamedEntityInfo.getOrganizations().size();
   }

}
