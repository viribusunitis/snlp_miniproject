package de.upb.vu.fc.util;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * This util class offers convenience methods for IO.
 */
public class IOUtils {


   /**
    * Hides the public constructor.
    */
   private IOUtils() {
   }


   /**
    * Returns the containing string of the given file.
    * 
    * @param filePath The path to file.
    * @return The content of the given file.
    * @throws IOException If an I/O error occurs while opening the file.
    */
   public static String readStringFromFile(final String filePath) throws IOException {
      StringJoiner joiner = new StringJoiner("\n");
      try (Stream<String> stream = Files.lines(Paths.get(filePath), Charset.defaultCharset())) {
         for (String line : stream.collect(Collectors.toList())) {
            joiner.add(line);
         }
      }
      return joiner.toString();
   }


   public static String readStringFromInputStream(final InputStream inputStream) throws IOException {
      StringJoiner joiner = new StringJoiner("\n");
      try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
         String line;
         while ((line = reader.readLine()) != null) {
            joiner.add(line);
         }
      }
      return joiner.toString();
   }


   /**
    * Writes the given {@code content} to the given file.
    * 
    * @param file The {@link File} to write the given {@code content} to.
    * @param content The content to write in the given {@code file}.
    * @throws IOException If an I/O error occurs.
    */
   public static void writeToFile(File file, String content) throws IOException {
      try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
         writer.write(content);
      }
   }


   /**
    * Writes the given {@code content} to the given file.
    * 
    * @param file The {@link File} to write the given {@code content} to.
    * @param content The content to write in the given {@code file}.
    * @throws IOException If an I/O error occurs.
    */
   public static void writeToFileCreatingMissingDirectories(File file, String content) throws IOException {
      File directory = file.getParentFile();
      if (directory != null) {
         directory.mkdirs();
      }
      writeToFile(file, content);
   }


}
