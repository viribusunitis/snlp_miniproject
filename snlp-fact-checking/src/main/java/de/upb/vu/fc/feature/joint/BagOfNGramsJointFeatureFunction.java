package de.upb.vu.fc.feature.joint;


import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.feature.article.ArticleNGramHashedFeatureFunction;
import de.upb.vu.fc.feature.fact.FactNGramHashedFeatureFunction;
import de.upb.vu.fc.math.RandomGenerator;
import de.upb.vu.fc.math.linearalgebra.Vector;
import de.upb.vu.fc.util.VectorUtil;
import de.upb.vu.fc.wikipedia.RawWikipediaArticle;


public class BagOfNGramsJointFeatureFunction implements JointFactArticleFeatureFunction {

   private ArticleNGramHashedFeatureFunction articleFeatureFunction;
   private FactNGramHashedFeatureFunction factFeatureFunction;

   private int lengthOfResultingFactFeatureVector;


   public BagOfNGramsJointFeatureFunction(int lengthOfResultingFactFeatureVector, int lengthOfResultingArticleFeatureVector,
         int minimumGramLength, int maximumGramLength) {
      this.articleFeatureFunction = new ArticleNGramHashedFeatureFunction((int) RandomGenerator.getSeed(),
            lengthOfResultingArticleFeatureVector, minimumGramLength, maximumGramLength);
      this.factFeatureFunction = new FactNGramHashedFeatureFunction((int) RandomGenerator.getSeed(), lengthOfResultingFactFeatureVector,
            minimumGramLength, maximumGramLength);
      this.lengthOfResultingFactFeatureVector = lengthOfResultingArticleFeatureVector;
   }


   @Override
   public Vector generateFeatureVector(Fact fact, RawWikipediaArticle article) {
      Vector factAsFeatureVector = factFeatureFunction.generateFeatureVector(fact);
      Vector articleAsFeatureVector = articleFeatureFunction.generateFeatureVector(article);
      return VectorUtil.mergeVectors(factAsFeatureVector, articleAsFeatureVector);
   }


   @Override
   public int getFeatureVectorLength() {
      return lengthOfResultingFactFeatureVector;
   }

}
