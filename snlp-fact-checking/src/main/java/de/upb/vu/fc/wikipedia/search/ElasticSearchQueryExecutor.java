package de.upb.vu.fc.wikipedia.search;


import de.upb.vu.fc.wikipedia.importer.elastic.ElasticSearchException;
import de.upb.vu.fc.wikipedia.search.query.ElasticSearchQuery;


public interface ElasticSearchQueryExecutor {

   public ElasticSearchResultSet executeQuery(ElasticSearchQuery searchQuery) throws ElasticSearchException;


   public void teardown();
}
