package de.upb.vu.fc.evaluation.metric;


import java.util.List;

import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.fact.FactDataset;


public abstract class AbstractMetric implements Metric {

   @Override
   public double getLoss(FactDataset factDataset, List<Double> computedScores) {
      if (factDataset.getSize() != computedScores.size()) {
         throw new IllegalArgumentException("Dataset and computed scores have a different size!");
      }

      double sumOfLosses = 0;
      for (int i = 0; i < factDataset.getSize(); i++) {
         Fact fact = factDataset.getFacts().get(i);
         double computedScore = computedScores.get(i);
         sumOfLosses += getLoss(fact, computedScore);
      }
      return sumOfLosses / factDataset.getSize();
   }

}
