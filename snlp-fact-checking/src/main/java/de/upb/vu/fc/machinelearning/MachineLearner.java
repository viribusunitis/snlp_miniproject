package de.upb.vu.fc.machinelearning;


import java.util.List;

import de.upb.vu.fc.math.linearalgebra.Vector;


public interface MachineLearner {

   public void train(List<Vector> instances, List<Double> confidenceScores);


   public double predict(Vector instance);


   public List<Double> predict(List<Vector> instances);

}
