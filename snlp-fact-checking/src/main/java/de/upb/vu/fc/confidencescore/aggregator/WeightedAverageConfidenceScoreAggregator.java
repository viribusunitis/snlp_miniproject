package de.upb.vu.fc.confidencescore.aggregator;


import java.util.List;

import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.util.datastructure.Pair;
import de.upb.vu.fc.wikipedia.search.ElasticSearchResult;


public class WeightedAverageConfidenceScoreAggregator extends AbstractConfidenceScoreAggregator {

   public WeightedAverageConfidenceScoreAggregator() {
   }


   @Override
   public double aggregateScores(Fact fact, List<Pair<ElasticSearchResult, Double>> resultAndScoreList) {
      double scoreSumOfSearchResults = getScoreSumOfSearchResults(resultAndScoreList);
      return resultAndScoreList.stream().mapToDouble(pair -> (pair.getFirst().getScore() / scoreSumOfSearchResults) * pair.getSecond())
            .average().getAsDouble();
   }


   public double getScoreSumOfSearchResults(List<Pair<ElasticSearchResult, Double>> resultAndScoreList) {
      return resultAndScoreList.stream().mapToDouble(pair -> pair.getFirst().getScore()).sum();
   }

}
