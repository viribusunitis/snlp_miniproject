package de.upb.vu.fc.wikipedia.importer.elastic;


public class ElasticSearchException extends Exception {

   public ElasticSearchException(String message) {
      super(message);
   }


   public ElasticSearchException(String message, Throwable cause) {
      super(message, cause);
   }
}
