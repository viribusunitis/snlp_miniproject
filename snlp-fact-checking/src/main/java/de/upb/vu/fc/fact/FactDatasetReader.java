package de.upb.vu.fc.fact;


import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import de.upb.vu.fc.util.IOUtils;


public class FactDatasetReader {

   public String pathToDataset;


   public FactDataset readTrainingDataset(InputStream trainingDatasetInputStream) throws IOException {
      this.pathToDataset = trainingDatasetInputStream.toString();

      String datasetAsString = IOUtils.readStringFromInputStream(trainingDatasetInputStream);
      return readDatasetFromString(datasetAsString);
   }


   public FactDataset readTestDataset(String pathToDataset) throws IOException {
      this.pathToDataset = pathToDataset;
      String datasetAsString = IOUtils.readStringFromFile(pathToDataset);

      return readDatasetFromString(datasetAsString);
   }


   private FactDataset readDatasetFromString(String datasetAsString) {
      FactDataset dataset = new FactDataset(this.pathToDataset);
      try (Scanner scanner = new Scanner(datasetAsString)) {

         // skip first line, as it is the header
         String currentLine = scanner.nextLine();

         int currentLineNumber = 1;
         while (scanner.hasNextLine()) {
            currentLine = scanner.nextLine();
            Fact currentFact = createFactFromLine(currentLine, currentLineNumber);
            dataset.addFact(currentFact);
            currentLineNumber++;
         }
      }
      return dataset;
   }


   private Fact createFactFromLine(String line, int lineNumber) {
      String[] splitLine = line.split("\t");
      if (splitLine.length != 2 && splitLine.length != 3) {
         throw new IllegalArgumentException(
               String.format("Given dataset (%s) features a wrong format in line %d.", pathToDataset, lineNumber));
      }
      try {
         String id = splitLine[0];
         String sentence = splitLine[1];
         double confidenceScore = 0;
         if (splitLine.length == 3) {
            confidenceScore = Double.parseDouble(splitLine[2]);
         }
         return new Fact(id, sentence, confidenceScore);
      } catch (NumberFormatException e) {
         throw new IllegalArgumentException(
               String.format("Given dataset (%s) features a wrong format in line %d.", pathToDataset, lineNumber), e);
      }
   }
}
