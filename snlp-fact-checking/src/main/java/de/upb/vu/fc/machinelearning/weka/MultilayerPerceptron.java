package de.upb.vu.fc.machinelearning.weka;


public class MultilayerPerceptron extends AbstractWekaMachineLearner {

   public MultilayerPerceptron(int amountOfAttributes, int amountOfTargetValues) {
      super(initializeWekaClassifier(0.1, 0.2, 2000, "1"), amountOfAttributes, amountOfTargetValues);
   }


   public MultilayerPerceptron(double learningRate, double momentum, int trainingTime, String hiddenLayers, int amountOfAttributes,
         int amountOfTargetValues) {
      super(initializeWekaClassifier(learningRate, momentum, trainingTime, hiddenLayers), amountOfAttributes, amountOfTargetValues);
   }


   private static weka.classifiers.functions.MultilayerPerceptron initializeWekaClassifier(double learningRate, double momentum,
         int trainingTime, String hiddenLayers) {
      weka.classifiers.functions.MultilayerPerceptron multilayerPerceptron = new weka.classifiers.functions.MultilayerPerceptron();
      multilayerPerceptron.setLearningRate(learningRate);
      multilayerPerceptron.setMomentum(momentum);
      multilayerPerceptron.setTrainingTime(trainingTime);
      multilayerPerceptron.setHiddenLayers(hiddenLayers);
      return multilayerPerceptron;
   }

}
