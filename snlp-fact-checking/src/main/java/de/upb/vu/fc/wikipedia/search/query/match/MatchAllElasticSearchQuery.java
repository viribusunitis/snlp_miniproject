package de.upb.vu.fc.wikipedia.search.query.match;


import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import de.upb.vu.fc.wikipedia.search.query.AbstractElasticSearchQuery;


public class MatchAllElasticSearchQuery extends AbstractElasticSearchQuery {

   @Override
   public QueryBuilder getQueryBuilder() {
      return QueryBuilders.matchAllQuery();
   }


   @Override
   public QueryBuilder getPostFilter() {
      return null;
   }

}
