package de.upb.vu.fc.wikipedia.jsonexport;


import java.io.File;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.upb.vu.fc.util.IOUtils;
import de.upb.vu.fc.wikipedia.RawWikipediaArticle;
import de.upb.vu.fc.wikipedia.WikipediaArticleHandler;
import de.upb.vu.fc.wikipedia.preprocessing.WikipediaArticlePreprocessor;


public class JsonExportArticleHandler implements WikipediaArticleHandler {

   private static final Logger LOGGER = LoggerFactory.getLogger(JsonExportArticleHandler.class);

   private String exportPath;
   private List<WikipediaArticlePreprocessor> articlePreprocessors;
   private ObjectMapper objectMapper;


   public JsonExportArticleHandler(String exportPath, List<WikipediaArticlePreprocessor> articlePreprocessors) {
      super();
      this.exportPath = exportPath;
      this.articlePreprocessors = articlePreprocessors;
      this.objectMapper = new ObjectMapper();
   }


   @Override
   public void handleArticle(RawWikipediaArticle article) {
      if (shouldArticleBeProcessed(article)) {
         preprocessArticle(article);
         exportArticleToJsonFile(article);
      }
   }


   private void preprocessArticle(RawWikipediaArticle article) {
      articlePreprocessors.forEach(processor -> processor.process(article));
   }


   private void exportArticleToJsonFile(RawWikipediaArticle article) {
      try {
         String articleAsJson = objectMapper.writeValueAsString(article);
         File articleFile = new File(exportPath + "/" + article.getId() + ".json");
         IOUtils.writeToFileCreatingMissingDirectories(articleFile, articleAsJson);
      } catch (IOException e) {
         LOGGER.error("Could not export article {} to json.", article, e);
      }
   }


   private boolean shouldArticleBeProcessed(RawWikipediaArticle article) {
      if (article.getText().startsWith("#REDIRECT")) {
         return false;
      }
      return true;
   }


   @Override
   public void teardown() {
      // nothing to do here
   }

}
