package de.upb.vu.fc.feature.article;


import java.util.Collection;

import de.upb.vu.fc.feature.DefaultFeatureHasher;
import de.upb.vu.fc.math.linearalgebra.Vector;
import de.upb.vu.fc.nlp.NGramCreator;
import de.upb.vu.fc.wikipedia.RawWikipediaArticle;


public class ArticleNGramHashedFeatureFunction implements ArticleFeatureFunction {

   private DefaultFeatureHasher featureHasher;
   private NGramCreator nGramCreator;


   public ArticleNGramHashedFeatureFunction(int seed, int lengthOfResultingVector, int minimumGramLength, int maximumGramLength) {
      this.featureHasher = new DefaultFeatureHasher(seed, lengthOfResultingVector);
      this.nGramCreator = new NGramCreator(minimumGramLength, maximumGramLength);
   }


   @Override
   public Vector generateFeatureVector(RawWikipediaArticle article) {
      Collection<String> extractedNGrams = nGramCreator.extractNGrams(article.getText());
      String[] input = extractedNGrams.toArray(new String[extractedNGrams.size()]);
      return featureHasher.hash(input);
   }

}
