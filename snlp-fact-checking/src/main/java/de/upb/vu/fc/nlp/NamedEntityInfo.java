package de.upb.vu.fc.nlp;


import java.util.List;


public class NamedEntityInfo {

   private String analyzedText;

   private List<String> persons;
   private List<String> locations;
   private List<String> organizations;


   public NamedEntityInfo(String analyzedText, List<String> persons, List<String> locations, List<String> organizations) {
      super();
      this.analyzedText = analyzedText;
      this.persons = persons;
      this.locations = locations;
      this.organizations = organizations;
   }


   public String getAnalyzedText() {
      return analyzedText;
   }


   public List<String> getPersons() {
      return persons;
   }


   public List<String> getLocations() {
      return locations;
   }


   public List<String> getOrganizations() {
      return organizations;
   }


   @Override
   public String toString() {
      return "NamedEntityInfo [analyzedText=" + analyzedText + ", persons=" + persons + ", locations=" + locations + ", organizations="
            + organizations + "]";
   }


}
