package de.upb.vu.fc.confidencescore.aggregator;


import java.util.List;

import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.util.datastructure.Pair;
import de.upb.vu.fc.wikipedia.search.ElasticSearchResult;


public interface ConfidenceScoreAggregator {

   public double aggregateScores(Fact fact, List<Pair<ElasticSearchResult, Double>> resultAndScoreList);

}
