package de.upb.vu.fc.confidencescore.computer.ml;


import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.wikipedia.RawWikipediaArticle;


public class TrainingInstance {

   private Fact fact;
   private RawWikipediaArticle article;
   private double confidenceScore;


   public TrainingInstance(Fact fact, RawWikipediaArticle article, double confidenceScore) {
      super();
      this.fact = fact;
      this.article = article;
      this.confidenceScore = confidenceScore;
   }


   public Fact getFact() {
      return fact;
   }


   public RawWikipediaArticle getArticle() {
      return article;
   }


   public double getConfidenceScore() {
      return confidenceScore;
   }

}
