package de.upb.vu.fc.feature.fact;

import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.math.linearalgebra.Vector;

public interface FactFeatureFunction {

	public Vector generateFeatureVector(Fact fact);
}
