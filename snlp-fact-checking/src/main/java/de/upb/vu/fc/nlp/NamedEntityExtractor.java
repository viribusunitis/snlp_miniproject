package de.upb.vu.fc.nlp;


import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringJoiner;

import de.upb.vu.fc.util.StringUtil;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;


public class NamedEntityExtractor {

   private static final String PERSON_TAG = "PERSON";
   private static final String LOCATION_TAG = "LOCATION";
   private static final String ORGANIZATION_TAG = "ORGANIZATION";

   private StanfordCoreNLP pipeline;


   public NamedEntityExtractor() {
      Properties props = new Properties();
      props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner");

      // pipeline = new StanfordCoreNLPClient(props, "139.18.2.39", 9000, 4);
      pipeline = new StanfordCoreNLP(props);
   }


   public NamedEntityInfo extractEntities(String text) {
      Annotation document = new Annotation(text.replaceAll(StringUtil.REGEX_PUNCTUATION, StringUtil.EMPTY_STRING));
      pipeline.annotate(document);

      List<String> persons = new ArrayList<>();
      List<String> locations = new ArrayList<>();
      List<String> organizations = new ArrayList<>();

      List<CoreMap> sentences = document.get(SentencesAnnotation.class);
      for (CoreMap sentence : sentences) {
         StringJoiner wordJoiner = new StringJoiner(" ");
         String lastTag = null;
         for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
            String word = token.get(TextAnnotation.class);
            String namedEntityTag = token.get(NamedEntityTagAnnotation.class);
            if (lastTag != null && !lastTag.equals(namedEntityTag)) {
               String wordSequence = wordJoiner.toString();
               if (lastTag.equalsIgnoreCase(PERSON_TAG)) {
                  persons.add(wordSequence);
               } else if (lastTag.equalsIgnoreCase(LOCATION_TAG)) {
                  locations.add(wordSequence);
               } else if (lastTag.equalsIgnoreCase(ORGANIZATION_TAG)) {
                  organizations.add(wordSequence);
               }
               wordJoiner = new StringJoiner(" ");
            }
            lastTag = namedEntityTag;
            wordJoiner.add(word);
         }
         if (lastTag != null) {
            String wordSequence = wordJoiner.toString();
            if (lastTag.equalsIgnoreCase(PERSON_TAG)) {
               persons.add(wordSequence);
            } else if (lastTag.equalsIgnoreCase(LOCATION_TAG)) {
               locations.add(wordSequence);
            } else if (lastTag.equalsIgnoreCase(ORGANIZATION_TAG)) {
               organizations.add(wordSequence);
            }
         }
      }
      return new NamedEntityInfo(text, persons, locations, organizations);
   }

}
