package de.upb.vu.fc.confidencescore.computer.ml;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.upb.vu.fc.confidencescore.computer.AbstractConfidenceScoreComputer;
import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.fact.FactDataset;
import de.upb.vu.fc.feature.joint.JointFactArticleFeatureFunction;
import de.upb.vu.fc.machinelearning.MachineLearner;
import de.upb.vu.fc.math.linearalgebra.Vector;
import de.upb.vu.fc.wikipedia.RawWikipediaArticle;
import de.upb.vu.fc.wikipedia.search.ElasticSearchResult;


public class MachineLearningBasedConfidenceScoreComputer extends AbstractConfidenceScoreComputer {

   private static final Logger LOGGER = LoggerFactory.getLogger(MachineLearningBasedConfidenceScoreComputer.class);

   private MachineLearner machineLearner;
   private JointFactArticleFeatureFunction jointFeatureFunction;
   private String pathToTrainingInstancesFile;


   public MachineLearningBasedConfidenceScoreComputer(MachineLearner machineLearner, JointFactArticleFeatureFunction jointFeatureFunction,
         String pathToTrainingInstancesFile) {
      this.machineLearner = machineLearner;
      this.jointFeatureFunction = jointFeatureFunction;
      this.pathToTrainingInstancesFile = pathToTrainingInstancesFile;
   }


   @Override
   public void setup(FactDataset factDataset) {
      TrainingDatasetReader reader = new TrainingDatasetReader(factDataset);

      try {
         TrainingDataset trainingDataset = reader.read(pathToTrainingInstancesFile);
         LOGGER.debug("Training with {} instances.", trainingDataset.getTrainingInstances().size());
         List<Vector> instances = getFactDatasetAsFactFeatureVectors(trainingDataset);
         List<Double> confidenceScores = getFactDatasetAsTargetValues(trainingDataset);
         machineLearner.train(instances, confidenceScores);
      } catch (IOException e) {
         LOGGER.error("Training dataset file {} could not be read.", pathToTrainingInstancesFile, e);
         throw new RuntimeException("Training dataset file " + pathToTrainingInstancesFile + " could not be read.");
      }
   }


   public List<Vector> getFactDatasetAsFactFeatureVectors(TrainingDataset trainingDataset) {
      List<Vector> instances = new ArrayList<>();
      for (TrainingInstance trainingInstance : trainingDataset.getTrainingInstances()) {
         Vector mergedVector = generateFactAndArticleVector(trainingInstance.getFact(), trainingInstance.getArticle());
         instances.add(mergedVector);
      }
      return instances;
   }


   public List<Double> getFactDatasetAsTargetValues(TrainingDataset trainingDataset) {
      List<Double> confidenceScores = new ArrayList<>();
      for (TrainingInstance instance : trainingDataset.getTrainingInstances()) {
         confidenceScores.add(instance.getConfidenceScore());
      }
      return confidenceScores;
   }


   @Override
   public double computeScore(Fact fact, ElasticSearchResult result) {
      Vector mergedVector = generateFactAndArticleVector(fact, result.getArticle());
      return machineLearner.predict(mergedVector);
   }


   private Vector generateFactAndArticleVector(Fact fact, RawWikipediaArticle article) {
      Vector vector = jointFeatureFunction.generateFeatureVector(fact, article);
      LOGGER.trace("Generated feature vector {} for {} and article {}.", vector, fact, article.getId());
      return vector;
   }

}
