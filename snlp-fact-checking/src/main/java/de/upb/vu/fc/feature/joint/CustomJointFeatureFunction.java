package de.upb.vu.fc.feature.joint;


import java.util.Collection;

import org.apache.commons.lang3.StringUtils;

import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.math.linearalgebra.DenseDoubleVector;
import de.upb.vu.fc.math.linearalgebra.Vector;
import de.upb.vu.fc.nlp.LevenstheinDistanceComputer;
import de.upb.vu.fc.nlp.NGramCreator;
import de.upb.vu.fc.nlp.NamedEntityExtractor;
import de.upb.vu.fc.nlp.NamedEntityInfo;
import de.upb.vu.fc.util.StringUtil;
import de.upb.vu.fc.wikipedia.RawWikipediaArticle;


public class CustomJointFeatureFunction implements JointFactArticleFeatureFunction {

   private NamedEntityExtractor namedEntityExtractor;

   private Fact fact;
   private RawWikipediaArticle article;
   private String cleanedArticleText;
   private String cleanedSentence;

   private NamedEntityInfo factNamedEntityInfo;
   private LevenstheinDistanceComputer levenstheinDistance;
   private NGramCreator nGramCreator;


   public CustomJointFeatureFunction() {
      namedEntityExtractor = new NamedEntityExtractor();
      levenstheinDistance = new LevenstheinDistanceComputer();
      nGramCreator = new NGramCreator(1);
   }


   @Override
   public Vector generateFeatureVector(Fact fact, RawWikipediaArticle article) {
      this.fact = fact;
      this.article = article;
      this.cleanedArticleText = cleanString(article.getText());
      this.cleanedSentence = cleanString(fact.getSentence());
      factNamedEntityInfo = namedEntityExtractor.extractEntities(fact.getSentence().replaceAll("'[a-zA-Z]", ""));

      int amountOfFeatures = getFeatureVectorLength();
      Vector featureVector = new DenseDoubleVector(amountOfFeatures);
      int index = 0;
      featureVector.setValue(index, getPercentageOfIncludedPersons());
      index++;
      featureVector.setValue(index, getPercentageOfIncludedLocationsFeature());
      index++;
      featureVector.setValue(index, getPercentageOfIncludedOrganizationsFeature());
      index++;
      featureVector.setValue(index, getAverageLevenstheinDistanceBetweenTitleAndEntities());
      index++;
      featureVector.setValue(index, getAverageUniGramFrequency());

      return featureVector;
   }


   private String cleanString(String string) {
      return string.replaceAll("'[a-zA-Z]", "").replaceAll(StringUtil.REGEX_PUNCTUATION, StringUtil.EMPTY_STRING)
            .replaceAll("\n", StringUtil.SINGLE_WHITESPACE).toLowerCase();
   }


   private double getPercentageOfIncludedPersons() {
      if (factNamedEntityInfo.getPersons().size() == 0) {
         return 1;
      }
      int amountOfPersonsIncluded = 0;
      for (String person : factNamedEntityInfo.getPersons()) {
         if (cleanedArticleText.contains(person.toLowerCase())) {
            amountOfPersonsIncluded++;
         }
      }
      return amountOfPersonsIncluded / (double) factNamedEntityInfo.getPersons().size();
   }


   private double getPercentageOfIncludedLocationsFeature() {
      if (factNamedEntityInfo.getLocations().size() == 0) {
         return 1;
      }
      int amountOfLocationsIncluded = 0;
      for (String location : factNamedEntityInfo.getLocations()) {
         if (cleanedArticleText.contains(location.toLowerCase())) {
            amountOfLocationsIncluded++;
         }
      }
      return amountOfLocationsIncluded / (double) factNamedEntityInfo.getLocations().size();
   }


   private double getPercentageOfIncludedOrganizationsFeature() {
      if (factNamedEntityInfo.getOrganizations().size() == 0) {
         return 1;
      }
      int amountOfOrganizationsIncluded = 0;
      for (String organization : factNamedEntityInfo.getOrganizations()) {
         if (cleanedArticleText.contains(organization.toLowerCase())) {
            amountOfOrganizationsIncluded++;
         }
      }
      return amountOfOrganizationsIncluded / (double) factNamedEntityInfo.getOrganizations().size();
   }


   private double getAverageLevenstheinDistanceBetweenTitleAndEntities() {
      int totalSumOfEntities = factNamedEntityInfo.getOrganizations().size() + factNamedEntityInfo.getLocations().size()
            + factNamedEntityInfo.getPersons().size();
      if (totalSumOfEntities > 0) {
         double distance = 0;
         String title = article.getTitle();
         for (String entity : factNamedEntityInfo.getOrganizations()) {
            distance += levenstheinDistance.computeDistance(title, entity);
         }
         for (String entity : factNamedEntityInfo.getLocations()) {
            distance += levenstheinDistance.computeDistance(title, entity);
         }
         for (String entity : factNamedEntityInfo.getPersons()) {
            distance += levenstheinDistance.computeDistance(title, entity);
         }
         return distance / totalSumOfEntities;
      }
      return 0;
   }


   private double getAverageUniGramFrequency() {
      int textSize = cleanedArticleText.split(" ").length;
      Collection<String> uniGrams = nGramCreator.extractNGrams(cleanedSentence);
      double totalCount = 0;
      for (String uniGram : uniGrams) {
         totalCount += StringUtils.countMatches(cleanedArticleText, uniGram);
      }
      return totalCount / textSize;
   }


   @Override
   public int getFeatureVectorLength() {
      return 5;
   }

}
