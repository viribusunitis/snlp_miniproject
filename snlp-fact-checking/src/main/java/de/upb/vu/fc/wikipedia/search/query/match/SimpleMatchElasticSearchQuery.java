package de.upb.vu.fc.wikipedia.search.query.match;


import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.wikipedia.importer.elastic.ElasticSearchSettings;
import de.upb.vu.fc.wikipedia.search.query.AbstractElasticSearchQuery;


public class SimpleMatchElasticSearchQuery extends AbstractElasticSearchQuery {

   private Fact fact;


   public SimpleMatchElasticSearchQuery(Fact fact) {
      this.fact = fact;
   }


   @Override
   public QueryBuilder getQueryBuilder() {
      return QueryBuilders.matchQuery(ElasticSearchSettings.TEXT_FIELD_NAME, fact.getSentence());
   }


   @Override
   public QueryBuilder getPostFilter() {
      return null;
   }

}
