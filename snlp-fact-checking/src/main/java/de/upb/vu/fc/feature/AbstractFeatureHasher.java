package de.upb.vu.fc.feature;


import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;


public abstract class AbstractFeatureHasher implements FeatureHasher {

   private int seed;
   protected int lengthOfResultingVector;


   public AbstractFeatureHasher(int seed, int lengthOfResultingVector) {
      this.seed = seed;
      this.lengthOfResultingVector = lengthOfResultingVector;
   }


   protected int hash(String input) {
      // seed is any integer.
      HashFunction hf = Hashing.murmur3_32(seed);

      // key is the input int and hd is the hashed dimension.
      int hash = Hashing.consistentHash(hf.hashUnencodedChars(input), lengthOfResultingVector);
      return hash;
   }

}
