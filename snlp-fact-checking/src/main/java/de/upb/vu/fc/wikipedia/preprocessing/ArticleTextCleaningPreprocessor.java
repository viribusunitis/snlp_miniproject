package de.upb.vu.fc.wikipedia.preprocessing;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.upb.vu.fc.wikipedia.RawWikipediaArticle;


public class ArticleTextCleaningPreprocessor implements WikipediaArticlePreprocessor {

   private static final Logger LOGGER = LoggerFactory.getLogger(ArticleTextCleaningPreprocessor.class);

   private VuWikiClean textCleaner;


   public ArticleTextCleaningPreprocessor() {
      textCleaner = new VuWikiClean.Builder().build();
   }


   @Override
   public void process(RawWikipediaArticle article) {
      try {
         String cleanedArticleText = textCleaner.clean(article.getText());
         article.setText(cleanedArticleText);

         article.setTitle(getCleanedTitle(article));
         article.setId(getCleanedId(article));
      } catch (Exception exception) {
         // Note that this is not a good idea in general, but since the cleaning seems to work very
         // inconsistently, we will try this here.
         LOGGER.error("Could not clean article {}.", article, exception);
      }
   }


   private String getCleanedId(RawWikipediaArticle article) {
      return article.getId().replaceAll("\n", "").trim();
   }


   private String getCleanedTitle(RawWikipediaArticle article) {
      return article.getTitle().replaceAll("\n", "").trim();
   }


   @Override
   public WikipediaArticlePreprocessor copy() {
      return new ArticleTextCleaningPreprocessor();
   }

}
