package de.upb.vu.fc.util;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.upb.vu.fc.math.linearalgebra.DenseDoubleVector;
import de.upb.vu.fc.math.linearalgebra.Vector;


/**
 * This class provides static utility methods for {@link Vector}s.
 * 
 * @author Alexander Hetzer
 */
public class VectorUtil {

   private VectorUtil() {
   }


   /**
    * Extends the given {@link Vector} by the given entry.
    * 
    * @param vectorToExtend The {@link Vector} to be extended.
    * @param entry The entry by which the {@link Vector} should be extended.
    * @return The extended {@link Vector}.
    */
   public static Vector extendVectorByEntry(Vector vectorToExtend, double entry) {
      Vector vector = new DenseDoubleVector(vectorToExtend.length() + 1);
      for (int i = 0; i < vector.length() - 1; i++) {
         vector.setValue(i, vectorToExtend.getValue(i));
      }
      vector.setValue(vector.length() - 1, entry);
      return vector;
   }


   /**
    * Merges the {@link Vector}s in the given {@link List} into a single one.
    * 
    * @param vectors The {@link Vector}s to merge.
    * @return The merged {@link Vector}.
    */
   public static Vector mergeVectors(List<Vector> vectors) {
      int totalSize = vectors.stream().mapToInt(Vector::length).sum();
      Vector mergedVector = new DenseDoubleVector(totalSize);
      int position = 0;
      for (Vector vector : vectors) {
         for (int i = 0; i < vector.length(); i++) {
            mergedVector.setValue(position, vector.getValue(i));
            position++;
         }
      }
      return mergedVector;
   }


   /**
    * Merges the given {@link Vector}s into a single one.
    * 
    * @param vector1 The first {@link Vector} to merge.
    * @param vector2 The second {@link Vector} to merge.
    * @return The merged {@link Vector}.
    */
   public static Vector mergeVectors(Vector vector1, Vector vector2) {
      List<Vector> vectorList = new ArrayList<>();
      vectorList.add(vector1);
      vectorList.add(vector2);
      return mergeVectors(vectorList);
   }


   public static String getSerializedVector(Vector vector) {
      return Arrays.toString(vector.asArray());
   }


   public static Vector readVectorFromString(String vectorAsString) {
      return new DenseDoubleVector(parseDoubleArray(vectorAsString));
   }


   private static double[] parseDoubleArray(String input) {
      String beforeSplit = input.replaceAll("\\[|\\]|\\s", "");
      String[] split = beforeSplit.split("\\,");
      double[] result = new double[split.length];
      for (int i = 0; i < split.length; i++) {
         result[i] = Double.parseDouble(split[i]);
      }
      return result;
   }

}
