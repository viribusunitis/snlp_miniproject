package de.upb.vu.fc.multithreading;


import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ExceptionPrintingThreadPoolExecutor extends ThreadPoolExecutor {

   private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionPrintingThreadPoolExecutor.class);


   public ExceptionPrintingThreadPoolExecutor(int numberOfThreads) {
      super(numberOfThreads, numberOfThreads, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
   }


   @Override
   protected void afterExecute(Runnable r, Throwable t) {
      super.afterExecute(r, t);
      if (t == null && r instanceof Future<?>) {
         try {
            Future<?> future = (Future<?>) r;
            if (future.isDone()) {
               future.get();
            }
         } catch (CancellationException ce) {
            t = ce;
         } catch (ExecutionException ee) {
            t = ee.getCause();
         } catch (InterruptedException ie) {
            Thread.currentThread().interrupt(); // ignore/reset
         }
      }
      if (t != null) {
         LOGGER.error("Exception encountered by {}.", r, t);
      }
   }

}
