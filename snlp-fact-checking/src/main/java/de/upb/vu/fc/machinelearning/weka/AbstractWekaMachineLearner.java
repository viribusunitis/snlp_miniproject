package de.upb.vu.fc.machinelearning.weka;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.upb.vu.fc.machinelearning.MachineLearner;
import de.upb.vu.fc.math.linearalgebra.Vector;
import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;


public abstract class AbstractWekaMachineLearner implements MachineLearner {

   private static final Logger LOGGER = LoggerFactory.getLogger(AbstractWekaMachineLearner.class);

   private Classifier wekaClassifier;
   private ArrayList<Attribute> wekaAttributes;
   private Instances wekaDataset;


   public AbstractWekaMachineLearner(Classifier wekaClassifier, int amountOfAttributes, int amountOfTargetValues) {
      this.wekaClassifier = wekaClassifier;
      this.wekaAttributes = createDummyAttributes(amountOfAttributes, amountOfTargetValues);
   }


   @Override
   public void train(List<Vector> instances, List<Double> confidenceScores) {
      initWeka(instances, confidenceScores);
      try {
         wekaClassifier.buildClassifier(wekaDataset);
      } catch (Exception e) {
         LOGGER.error("Training the Weka Classifier failed.");
         throw new RuntimeException("Training the Weka Classifier failed.", e);
      }
   }


   public void initWeka(List<Vector> instances, List<Double> confidenceScores) {
      wekaDataset = new Instances("", wekaAttributes, instances.size());
      wekaDataset.setClassIndex(wekaAttributes.size() - 1);
      for (int i = 0; i < instances.size(); i++) {
         double[] actualFeatureVector = instances.get(i).asArray();
         DenseInstance wekaInstance = new DenseInstance(1, Arrays.copyOf(actualFeatureVector, wekaAttributes.size()));
         wekaDataset.add(wekaInstance);
         String classValue = String.valueOf((int) confidenceScores.get(i).doubleValue());
         wekaDataset.get(i).setClassValue(classValue);
      }
   }


   @Override
   public double predict(Vector instance) {
      DenseInstance wekaInstance = new DenseInstance(1, instance.asArray());
      wekaInstance.setDataset(wekaDataset);
      try {
         return wekaClassifier.distributionForInstance(wekaInstance)[1];
      } catch (Exception e) {
         LOGGER.error("Predicting on the Weka Classifier failed.");
         throw new RuntimeException("Predicting on the Weka Classifier failed.", e);
      }
   }


   @Override
   public List<Double> predict(List<Vector> instances) {
      List<Double> predictions = new ArrayList<>();
      for (int i = 0; i < instances.size(); i++) {
         predictions.add(predict(instances.get(i)));
      }
      return predictions;
   }


   private ArrayList<Attribute> createDummyAttributes(int amountOfAttributes, int amountOfTargetValues) {
      ArrayList<Attribute> dummyAttributes = new ArrayList<>(amountOfAttributes);
      for (int i = 0; i < amountOfAttributes; i++) {
         dummyAttributes.add(new Attribute("Attribute" + i));
      }

      List<String> targetValues = new ArrayList<>();
      for (int i = 0; i < amountOfTargetValues; i++) {
         targetValues.add(String.valueOf(i));
      }
      dummyAttributes.add(new Attribute("Attribute" + (amountOfAttributes), targetValues));

      return dummyAttributes;
   }

}
