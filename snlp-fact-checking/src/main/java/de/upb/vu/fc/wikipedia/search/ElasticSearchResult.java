package de.upb.vu.fc.wikipedia.search;


import de.upb.vu.fc.wikipedia.RawWikipediaArticle;


public class ElasticSearchResult {
   private RawWikipediaArticle article;
   private double score;


   public ElasticSearchResult(RawWikipediaArticle article, double score) {
      super();
      this.article = article;
      this.score = score;
   }


   public RawWikipediaArticle getArticle() {
      return article;
   }


   public double getScore() {
      return score;
   }


}
