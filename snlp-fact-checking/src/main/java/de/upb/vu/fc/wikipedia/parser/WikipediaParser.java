package de.upb.vu.fc.wikipedia.parser;


import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import de.upb.vu.fc.wikipedia.SaxXmlHandler;
import de.upb.vu.fc.wikipedia.WikipediaArticleHandler;


public class WikipediaParser extends AbstractWikipediaParser {

   private static final Logger LOGGER = LoggerFactory.getLogger(WikipediaParser.class);

   private XMLReader xmlReader;
   private List<WikipediaArticleHandler> articleHandlers;


   public WikipediaParser(File xmlDumpFile) throws MalformedURLException {
      super(xmlDumpFile);
      initializeXmlReader();
      this.articleHandlers = new LinkedList<>();
   }


   private void initializeXmlReader() {
      try {
         xmlReader = XMLReaderFactory.createXMLReader();
      } catch (SAXException e) {
         LOGGER.error("Could not initialize XML reader due to SAXException: ", e);
      }
   }


   @Override
   public void parse() {
      xmlReader.setContentHandler(new SaxXmlHandler(articleHandlers));
      try {
         xmlReader.parse(getInputSource());
      } catch (IOException e) {
         LOGGER.error("Could not parse dump due to IOException: ", e);
      } catch (SAXException e) {
         LOGGER.error("Could not parse dump due to SAXException: ", e);
      }
   }


   @Override
   public void addArticleHandler(WikipediaArticleHandler handler) {
      articleHandlers.add(handler);
   }


   @Override
   public void teardown() {
      articleHandlers.forEach(handler -> handler.teardown());
   }


}
