package de.upb.vu.fc.wikipedia.jsonexport;


import java.io.File;
import java.net.MalformedURLException;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.upb.vu.fc.wikipedia.WikipediaArticleHandler;
import de.upb.vu.fc.wikipedia.parser.WikipediaParser;
import de.upb.vu.fc.wikipedia.preprocessing.ArticleTextCleaningPreprocessor;
import de.upb.vu.fc.wikipedia.preprocessing.WikipediaArticlePreprocessor;


public class JsonExportRunner {

   private static final Logger LOGGER = LoggerFactory.getLogger(JsonExportRunner.class);

   private static final String XML_DUMP_FILE_PATH = "/Users/alexanderhetzer/Downloads/enwiki-20171120-pages-articles.xml.bz2";
   private static final String XML_DUMP_FILE_PATH_SERVER = "/home/viribusunitis/data/enwiki-20171120-pages-articles.xml.bz2";


   public static void main(String[] args) {
      String dumpFilePath = args[0];
      WikipediaParser parser;
      try {
         parser = new WikipediaParser(new File(dumpFilePath));

         WikipediaArticlePreprocessor cleaningPreprocessor = new ArticleTextCleaningPreprocessor();
         WikipediaArticleHandler articleHandler = new JsonExportArticleHandler("json_export", Arrays.asList(cleaningPreprocessor));

         parser.addArticleHandler(articleHandler);

         parser.parse();

      } catch (MalformedURLException e) {
         LOGGER.error("Could not read XML dump file.", e);
      }
   }


}
