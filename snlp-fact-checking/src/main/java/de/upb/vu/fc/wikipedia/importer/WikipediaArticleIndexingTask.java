package de.upb.vu.fc.wikipedia.importer;


import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;

import de.upb.vu.fc.wikipedia.RawWikipediaArticle;
import de.upb.vu.fc.wikipedia.importer.elastic.ElasticSearchSettings;
import de.upb.vu.fc.wikipedia.preprocessing.WikipediaArticlePreprocessor;


public class WikipediaArticleIndexingTask implements Runnable {

   private static final Logger LOGGER = LoggerFactory.getLogger(WikipediaArticleIndexingTask.class);

   private int workerId;

   private List<WikipediaArticlePreprocessor> articlePreprocessors;
   private List<RawWikipediaArticle> articles;

   private JsonSerializer serializer;
   private TransportClient elasticClient;


   public WikipediaArticleIndexingTask(int workerId, List<WikipediaArticlePreprocessor> articlePreprocessors,
         List<RawWikipediaArticle> articles) {
      super();
      this.workerId = workerId;
      this.articlePreprocessors = new ArrayList<>(articlePreprocessors.size());
      for (WikipediaArticlePreprocessor processor : articlePreprocessors) {
         this.articlePreprocessors.add(processor.copy());
      }
      this.articles = new ArrayList<>(articles);
      Collections.copy(this.articles, articles);

      this.serializer = new JsonSerializer();
   }


   @Override
   public void run() {
      try {
         setupElasticClient();

         for (RawWikipediaArticle article : articles) {
            preprocessArticle(article);
         }
         bulkIndexAllArticlesToElasticSearch();

         closeElasticClient();
         LOGGER.info("Task {}: Indexed {} articles.", workerId, articles.size());
      } catch (UnknownHostException e) {
         LOGGER.error("Worker {}: Could not connect to ES. ERROR: \n {}", workerId, e);
      } catch (JsonProcessingException e) {
         LOGGER.error("Worker {}: Could not serialize document. ERROR: \n {}", workerId, e);
      }
   }


   private void setupElasticClient() throws UnknownHostException {
      Settings settings = Settings.builder().put("cluster.name", ElasticSearchSettings.CLUSTER_NAME).build();
      elasticClient = new PreBuiltTransportClient(settings).addTransportAddress(
            new TransportAddress(InetAddress.getByName(ElasticSearchSettings.IP), ElasticSearchSettings.TRANSPORT_CLIENT_PORT));
   }


   private void closeElasticClient() {
      elasticClient.close();
   }


   private void preprocessArticle(RawWikipediaArticle article) {
      articlePreprocessors.forEach(preprocessor -> preprocessor.process(article));
   }


   private void bulkIndexAllArticlesToElasticSearch() throws JsonProcessingException {
      BulkRequestBuilder bulkRequest = elasticClient.prepareBulk();

      for (RawWikipediaArticle article : articles) {
         String articleAsJson = serializer.serializeArticleToJson(article);
         bulkRequest.add(elasticClient.prepareIndex(ElasticSearchSettings.INDEX_NAME, ElasticSearchSettings.TYPE_NAME)
               .setSource(articleAsJson, XContentType.JSON));
      }

      BulkResponse bulkResponse = bulkRequest.get();
      if (bulkResponse.hasFailures()) {
         for (BulkItemResponse response : bulkResponse) {
            if (response.isFailed()) {
               LOGGER.error("Error while indexing document {}.", response.getFailureMessage());
            }
         }
      }
   }

}
