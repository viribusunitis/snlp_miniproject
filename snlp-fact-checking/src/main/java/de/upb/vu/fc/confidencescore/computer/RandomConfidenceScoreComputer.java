package de.upb.vu.fc.confidencescore.computer;


import java.util.Random;

import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.fact.FactDataset;
import de.upb.vu.fc.math.RandomGenerator;
import de.upb.vu.fc.wikipedia.search.ElasticSearchResult;


public class RandomConfidenceScoreComputer extends AbstractConfidenceScoreComputer {

   private Random randomGenerator;


   @Override
   public void setup(FactDataset trainingDataset) {
      randomGenerator = RandomGenerator.getRNG();
   }


   @Override
   public double computeScore(Fact fact, ElasticSearchResult result) {
      return randomGenerator.nextDouble();
   }

}
