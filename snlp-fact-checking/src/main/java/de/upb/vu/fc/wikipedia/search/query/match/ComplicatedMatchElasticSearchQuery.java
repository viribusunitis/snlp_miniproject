package de.upb.vu.fc.wikipedia.search.query.match;


import java.util.HashSet;
import java.util.Set;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import de.upb.vu.fc.fact.Fact;
import de.upb.vu.fc.nlp.NamedEntityExtractor;
import de.upb.vu.fc.nlp.NamedEntityInfo;
import de.upb.vu.fc.wikipedia.importer.elastic.ElasticSearchSettings;
import de.upb.vu.fc.wikipedia.search.query.AbstractElasticSearchQuery;


public class ComplicatedMatchElasticSearchQuery extends AbstractElasticSearchQuery {

   private Fact fact;
   private Set<String> entitySet;


   public ComplicatedMatchElasticSearchQuery(Fact fact, NamedEntityExtractor extractor) {
      this.fact = fact;
      entitySet = new HashSet<>();
      NamedEntityInfo info = extractor.extractEntities(fact.getSentence());
      entitySet.addAll(info.getLocations());
      entitySet.addAll(info.getOrganizations());
      entitySet.addAll(info.getPersons());
   }


   @Override
   public QueryBuilder getQueryBuilder() {
      BoolQueryBuilder overallBoolQuery = QueryBuilders.boolQuery();
      QueryBuilder simpleSentenceMatch = QueryBuilders.matchQuery(ElasticSearchSettings.TEXT_FIELD_NAME, fact.getSentence());
      overallBoolQuery.should(simpleSentenceMatch);
      for (String entity : entitySet) {
         QueryBuilder entityInTitleSearchQuery = QueryBuilders.matchQuery(ElasticSearchSettings.TITLE_FIELD_NAME, entity);
         overallBoolQuery.should(entityInTitleSearchQuery);
      }
      return overallBoolQuery;
   }


   @Override
   public QueryBuilder getPostFilter() {
      return null;
   }


   @Override
   public int getSize() {
      return 10;
   }


}
