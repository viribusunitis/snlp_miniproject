package de.upb.vu.fc.wikipedia.search.query;


import org.elasticsearch.index.query.QueryBuilder;


public interface ElasticSearchQuery {


   public QueryBuilder getQueryBuilder();


   public QueryBuilder getPostFilter();


   public int getOffset();


   public int getSize();
}
