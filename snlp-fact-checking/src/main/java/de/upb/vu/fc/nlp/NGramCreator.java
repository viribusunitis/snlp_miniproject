package de.upb.vu.fc.nlp;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.StringJoiner;

import de.upb.vu.fc.util.StringUtil;


public class NGramCreator {

   private final int minimumGramLength;
   private final int maximumGramLength;


   public NGramCreator(int gramLength) {
      this(gramLength, gramLength);
   }


   public NGramCreator(int minimumGramLength, int maximumGramLength) {
      this.minimumGramLength = minimumGramLength;
      this.maximumGramLength = maximumGramLength;
   }


   public Collection<String> extractNGrams(String originalText) {
      List<String> nGrams = new ArrayList<>();
      String[] splitText = originalText.replaceAll(StringUtil.REGEX_PUNCTUATION, StringUtil.EMPTY_STRING).toLowerCase()
            .split(StringUtil.REGEX_WHITESPACE);

      for (int i = 0; i <= splitText.length - minimumGramLength; i++) {
         StringJoiner nGramStringBuilder = new StringJoiner(StringUtil.SINGLE_WHITESPACE);
         for (int j = 0; j < maximumGramLength && i + j < splitText.length; j++) {
            nGramStringBuilder.add(splitText[i + j]);
            int currentNGramCount = j + 1;
            if (maximumGramLength >= currentNGramCount && currentNGramCount >= minimumGramLength) {
               nGrams.add(nGramStringBuilder.toString());
            }
         }
      }

      return nGrams;
   }
}
