package de.upb.vu.fc.wikipedia;


public class RawWikipediaArticle {

   private String id;
   private String title;
   private String text;


   public RawWikipediaArticle() {
      super();
   }


   public RawWikipediaArticle(String title, String text, String id) {
      super();
      this.title = title;
      this.text = text;
      this.id = id;
   }


   public String getTitle() {
      return title;
   }


   public String getText() {
      return text;
   }


   public String getId() {
      return id;
   }


   public void setTitle(String title) {
      this.title = title;
   }


   public void setText(String text) {
      this.text = text;
   }


   public void setId(String id) {
      this.id = id;
   }


   @Override
   public String toString() {
      return "RawWikipediaArticle [id=" + id + ", title=" + title + ", text=" + text.substring(0, Math.min(200, text.length())) + "]";
   }


}
