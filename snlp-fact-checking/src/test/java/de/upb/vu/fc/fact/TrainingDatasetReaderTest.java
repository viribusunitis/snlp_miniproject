package de.upb.vu.fc.fact;


import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import de.upb.vu.fc.confidencescore.computer.ml.TrainingDataset;
import de.upb.vu.fc.confidencescore.computer.ml.TrainingDatasetReader;


public class TrainingDatasetReaderTest {

   @Test
   public void test() throws IOException {
      FactDatasetReader factReader = new FactDatasetReader();
      FactDataset factDataset = factReader.readTestDataset("src/test/resources/fact/train.tsv");

      TrainingDatasetReader trainingReader = new TrainingDatasetReader(factDataset);
      TrainingDataset traininDataset = trainingReader.read("fact/trainingFacts.tsv");
      assertEquals(64, traininDataset.size());

   }

}
