package de.upb.vu.fc.nlp;


import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;


public class NamedEntityExtractorTest {

   @Test
   public void testEntityExtraction() {
      NamedEntityExtractor extractor = new NamedEntityExtractor();
      NamedEntityInfo info = extractor.extractEntities("Barack Obama was born in Colorado");
      List<String> persons = Arrays.asList("Barack Obama");
      List<String> locations = Arrays.asList("Colorado");
      List<String> organizations = Arrays.asList();
      assertEquals(persons, info.getPersons());
      assertEquals(locations, info.getLocations());
      assertEquals(organizations, info.getOrganizations());


      NamedEntityInfo info2 = extractor.extractEntities("George W. Bush lives in New York");
      List<String> persons2 = Arrays.asList("George W Bush");
      List<String> locations2 = Arrays.asList("New York");
      List<String> organizations2 = Arrays.asList();
      assertEquals(persons2, info2.getPersons());
      assertEquals(locations2, info2.getLocations());
      assertEquals(organizations2, info2.getOrganizations());

      NamedEntityInfo info3 = extractor.extractEntities("Apple Inc. is a great company in Alabama.");
      List<String> persons3 = Arrays.asList();
      List<String> locations3 = Arrays.asList("Alabama");
      List<String> organizations3 = Arrays.asList("Apple Inc");
      assertEquals(persons3, info3.getPersons());
      assertEquals(locations3, info3.getLocations());
      assertEquals(organizations3, info3.getOrganizations());
   }
}
