package de.upb.vu.fc.nlp;


import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;

import de.upb.vu.fc.nlp.NGramCreator;


public class NGramCreatorTest {

   private static final String TEST_TEXT = "Obama is no longer the president of the United States. Donald J. Trump took over now.";


   @Test
   public void testUniGramCreation() {
      NGramCreator creator = new NGramCreator(1);
      Collection<String> actualUniGrams = creator.extractNGrams(TEST_TEXT);
      List<String> expectedUniGrams = Arrays.asList("obama", "is", "no", "longer", "the", "president", "of", "the", "united", "states",
            "donald", "j", "trump", "took", "over", "now");
      assertEquals(expectedUniGrams, actualUniGrams);
   }


   @Test
   public void testBiGramCreation() {
      NGramCreator creator = new NGramCreator(2);
      Collection<String> actualUniGrams = creator.extractNGrams(TEST_TEXT);
      List<String> expectedUniGrams = Arrays.asList("obama is", "is no", "no longer", "longer the", "the president", "president of",
            "of the", "the united", "united states", "states donald", "donald j", "j trump", "trump took", "took over", "over now");
      assertEquals(expectedUniGrams, actualUniGrams);
   }
}
