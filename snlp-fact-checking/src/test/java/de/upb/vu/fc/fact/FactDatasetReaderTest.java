package de.upb.vu.fc.fact;


import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;


public class FactDatasetReaderTest {

   private static final String PATH_TO_TRAINING_FACT_DATASET = "fact/train.tsv";
   private static final String PATH_TO_TRAINING_FACT_DATASET_MISSING_VALUES_IN_LINE = "fact/train_missing_values_in_line.tsv";
   private static final String PATH_TO_TRAINING_FACT_DATASET_WITH_WRONG_CONFIDENCE_SCORES = "fact/train_with_wrong_confidence_scores.tsv";

   private FactDatasetReader datasetReader;


   @Before
   public void initializeBeforeTests() {
      datasetReader = new FactDatasetReader();
   }


   @Test
   public void testReadingCorrectFile() throws IOException {
      InputStream trainingDatasetStream = FactDatasetReader.class.getClassLoader().getResourceAsStream(PATH_TO_TRAINING_FACT_DATASET);
      FactDataset dataset = datasetReader.readTrainingDataset(trainingDatasetStream);
      assertEquals(1301, dataset.getSize());

      Fact actualFact = dataset.getFacts().get(480);
      Fact expectedFact = new Fact("3487950", "The Neutronium Alchemist's author is Peter F. Hamilton.", 1.0);
      assertEquals(expectedFact, actualFact);

      Fact actualFact2 = dataset.getFacts().get(892);
      Fact expectedFact2 = new Fact("3238810", "Los Angeles Clippers is Rasheed Wallace's squad.", 0.0);
      assertEquals(expectedFact2, actualFact2);
   }


   @Test(expected = IllegalArgumentException.class)
   public void testReadingFileMissingValuesInLine() throws IOException {
      InputStream trainingDatasetStream = FactDatasetReader.class.getClassLoader()
            .getResourceAsStream(PATH_TO_TRAINING_FACT_DATASET_MISSING_VALUES_IN_LINE);
      FactDataset dataset = datasetReader.readTrainingDataset(trainingDatasetStream);
      assertEquals(1301, dataset.getSize());
   }


   @Test(expected = IllegalArgumentException.class)
   public void testReadingFileWithWrongConfidenceScores() throws IOException {
      InputStream trainingDatasetStream = FactDatasetReader.class.getClassLoader()
            .getResourceAsStream(PATH_TO_TRAINING_FACT_DATASET_WITH_WRONG_CONFIDENCE_SCORES);
      FactDataset dataset = datasetReader.readTrainingDataset(trainingDatasetStream);
      assertEquals(1301, dataset.getSize());
   }
}
