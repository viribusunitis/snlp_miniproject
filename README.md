# SNLP Mini-Project: Fact Checking (SNLP WS 17-18 UPB)

This repository contains the current status of the work on the mini-project for the course SNLP at the UPB in WS 17-18 of the group Viribus Unitis. 
The mini-project deals with fact checking. 

## Approach
For an explanation of the approach we follow, we refer the interested reader to our final presentation which is uploaded [HERE](https://bitbucket.org/viribusunitis/snlp_miniproject/raw/master/SNLP_FinalPresentation_ViribusUnitis.pdf).

## Prerequesites (Installation Requirements)
### Elasticsearch
This project relies on [Elasticsearch](https://www.elastic.co/de/products/elasticsearch) (ES) and thus you have to make sure to have an installation of ES (>= 6.0) on the system (or another server) where you want to run this fact checking engine. 

The settings the engine requires to communicate with ES are hardcoded as constants in the [`ElasticSearchSettings`](https://bitbucket.org/viribusunitis/snlp_miniproject/src/master/snlp-fact-checking/src/main/java/de/upb/vu/fc/wikipedia/importer/elastic/ElasticSearchSettings.java?at=master&fileviewer=file-view-default) class. If you install ES on another machine than where you want to run this engine, make sure to change the IP and the port accordingly. 

Furthermore the default settings for the cluster, the engine communicates with, are as follows:

```
CLUSTER_NAME = "viribus-unitis-fc-cluster";
```

Thus, either change the settings in the class named above or make sure that the ES instance you are using for this engine has the associated default cluster name.

Additionally by default the engine expects an index with `wikipedia` as identifier containing a type definition for `article`, featuring the following mapping:

```
{
	"article": { 
	    "properties": { 
	      "id":      { "type": "keyword" },  
	      "title":    { "type": "keyword"  }, 
	      "text":     { "type": "text"  }
	    }
	}
}
```

### Wikipedia Import
As the fact checking engine relies on Wikipedia in order to verify facts, the ES instance described above needs to be filled with the pages from an English Wikipedia dump. In order to do so, make sure that the ES instance is running under the parameters defined in the settings class noted above. If this is the case, you can run a full import of a dump by running the [`ElasticImportRunner`](https://bitbucket.org/viribusunitis/snlp_miniproject/src/master/snlp-fact-checking/src/main/java/de/upb/vu/fc/wikipedia/importer/elastic/ElasticImportRunner.java?at=master&fileviewer=file-view-default) with `s` as commandline argument. 

The runner assumes the dump to be located under the file defined by the `XML_DUMP_FILE_PATH_SERVER` constant. Please make sure to replace the path to the dump to the location where your dump lies. 

Dumps can be downloaded from [here](https://dumps.wikimedia.org/enwiki/). Please make sure to download a non-multistreamed dump as the importer cannot process a multistreamed dump. Thus make sure that the filename of the dump you download is of the form `enwiki-<DATE>-pages-articles.xml.bz2`.

Please note that the importer deletes any existing indices with the name defined in the settings (by default this is `wikipedia`) and thus all current data is lost. 

Futhermore please note that the import may take several hours - on our machines it tends to take roughly 8 hours.

Please note that if you run the engine on the server we were provided for the project, all prerequisites are already fulfilled and no Wikipedia import has to be done!

## Building and Running this Project
### Building
As the engine is a simple Maven project, it can be built by running 
```
mvn package
```
once the user navigated into the directory of the project.

The pom is configured in such a way to generate a single runnable jar containing all dependencies in the `target` directory of the project featuring the name `snlp-fact-checking-0.0.1-SNAPSHOT-jar-with-dependencies.jar`.

## Running an Evaluation
The dataset which is used to train the ML part of the approach is built into the jar and thus does not need to be passed as a parameter to the jar. The latest version of the dataset can be viewed [HERE](https://bitbucket.org/viribusunitis/snlp_miniproject/src/master/snlp-fact-checking/src/main/resources/data/confidence_score_computer_training_data.tsv?at=master&fileviewer=file-view-default). 

Once the jar is built, running an evaluation is as simple as running the jar. We suggest to run it with at least 4GB of RAM. Thus, the according command is:

```
java -Xms4096m -Xmx4096m -jar snlp-fact-checking-0.0.1-SNAPSHOT-jar-with-dependencies.jar data/test.tsv
```

The above command assumes that a dataset titled `test.tsv` is located in the same folder as the jar in a subdirectory called `data`. Generally you can change that parameter to the location of the dataset you want to evaluate.

Such a run takes several minutes (5-6 on the provided server on the provided datasets) and produces a directory called `evaluation` containing a file `result.ttl`, which is in the format requested by the project description.

Please note that the evaluation can deal with datasets which do have a confidence value and which do not have one. In case no confidene values for the facts are given, it is set to `0` and thus the metric results printed at the end of the evaluation are meaningless. If confidence values are provided, the metric results reflect the performance of the approach on the dataset.

## Branch Structure
We differentiate between 2 types of branches. Firstly, we have the `master` branch featuring the current state of the code. Secondly, we have `release` branches, which are prefixed with `release/` featuring the state of the code producing a specific evaluation result. This result is encoded in the branch name. For example, the code in the branch `release/auc_660-rmse_489` features an evaluation resulting in an AUC of 0.66 and an RMSE of 0.489 on the dataset provided with the project description.

## Remarks for the Advisors
As already discussed it would be best to evaluate the project on the server we were provided as it is already fully setup (with respect to ES and the Wikipedia import). The credentials should be available.

The easiest way to build the code of the master branch is to run the build script:
```
sh ~/run_full_build.sh
```

Running this script produces the according jar: 
```
~/experiments/snlp-fact-checking-latest.jar
```

This jar should be executed as described earlier.
